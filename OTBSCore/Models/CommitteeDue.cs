﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.Models
{
    public class CommitteeDue
    {
        [Key]
        public int SNo { get; set; }

        [Required]
        public int CommitteeId { get; set; }

        [Required]
        public int VehicleScheduleId { get; set; }

        [Required]
        [StringLength(10)]
        [Display(Name = "Phone Number")]
        public string PhNumber { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime Date { get; set; }

        [Display(Name ="Due Amount")]
        public int DueAmount { get; set; }

        public bool Cleared { get; set; }

        public virtual Committee Committee { get; set; }
        public virtual VehicleSchedule VehicleSchedule { get; set; }
    }
}
