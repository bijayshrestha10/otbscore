﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.Models
{
    public class Seat
    {
        public int Id { get; set; }

        public String SeatNumber { get; set; }
        public bool booked { get; set; }

        public int VehicleScheduleId { get; set; }

        public VehicleSchedule VehicleSchedule { get; set; }
    }
}
