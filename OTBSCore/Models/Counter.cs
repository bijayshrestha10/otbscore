﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.Models
{
    
    public class Counter
    {
        [Key]
        public int CounterId { get; set; }

        
        public int CommitteeId { get; set; }

        [Required]
        [StringLength(80)]
        public string Name { get; set; }

        [StringLength(50)]
        public string CounterUsername { get; set; }

        [Required]
        [StringLength(10)]
        [Display(Name="Phone Number")]
        public string PhNumber { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(20)]
        public string District { get; set; }

        [Required]
        [StringLength(20)]
        public string Municipality { get; set; }

        [Required]
        [StringLength(20)]
        public string LocalAddress { get; set; }

        [Required]
        public Status status { get; set; }

        //[Required]
        //public Member member { get; set; }

        [Display(Name="Commission %")]
        public int Commission { get; set; }//percentage

        public virtual Committee Committee { get; set; }

    }
}
