﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.Models
{
    public class TicketBooking
    {
        public int TicketBookingId { get; set; }
        public string TicketBookingCode { get; set; }
        public int CustomerId { get; set; }
        public string Contact { get; set; }
        public int NumberOfTickets { get; set; }
        public int TotalAmount { get; set; }
        public int VehicleRoutesId { get; set; }
        public int BookingPaymentId { get; set; }
        public string BookingType { get; set; }        // Either from counter or by customer itself - Used to calculate counter commission

    }
}
