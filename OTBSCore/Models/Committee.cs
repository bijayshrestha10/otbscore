﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.Models
{
    public enum Status
    {
        verified, unverified, suspended
    }

    public class Committee
    {
       
        [Key]
        public int CommitteeId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Username")]
        public string CommitteeUsername { get; set; }

        [Required]
        [StringLength(10)]
        [Display(Name="Phone Number")]
        public string PhNumber { get; set; }

        [Required]
        [StringLength(20)]
        public string District { get; set; }

        [Required]
        [StringLength(20)]
        public string Municipality  { get; set; }

        [Required]
        [StringLength(20)]
        public string LocalAddress { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public Status status { get; set; }
       


        public ICollection<Vehicle> vehicles { get; set; }
        public ICollection<Counter> counters { get; set; }
        //public ICollection<Member> members { get; set; }
        //public ICollection<Document> documents { get; set; }

    }
}
