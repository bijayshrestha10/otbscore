﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.Models
{
    public class Destination
    {
        [Required]
        public int DestinationId { get; set; }

        [Required]
        [StringLength(30)]
        [Display(Name ="Destination Name")]
        public string DestinationName { get; set; }

        [Required]
        [StringLength(20)]
        public string District { get; set; }


    }
}
