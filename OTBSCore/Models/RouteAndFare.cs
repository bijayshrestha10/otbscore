﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.Models
{
    public class RouteAndFare
    {
        [Key]
        public int RouteAndFareId { get; set; }

        [Required]
        [Display(Name ="Destination 1")]
        [StringLength(30)]
        public string Destination1 { get; set; }

        [Required]
        [Display(Name = "Destination 2")]
        [StringLength(30)]
        public string Destination2 { get; set; }

        [Required]
        public int Amount { get; set; }

        public int Distance { get; set; }



    }
}
