﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.Models
{
    public class VehicleSchedule
    {
        [Key]
        public int VehicleScheduleId { get; set; }

        public int CommitteeId { get; set; }

        public int VehicleId { get; set; }

        //[Required]
        //[StringLength(10)]
        //[RegularExpression(@"^[2][0][7-9][0-9][/]0[0-9]|1[0-1][/][0-2][1-9]|3[0-2]",ErrorMessage ="Format error!! Ex:2076/03/04")]
        //public String Date { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:yyyy/MM/dd}")]
        public DateTime Date { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString ="{0:hh:mm tt}")]
        public DateTime Time { get; set; }

        //[Required]
        //[StringLength(5)]
        //[RegularExpression(@"^0?[1-9]|1[0-2][:][0-5]?[0-9]",ErrorMessage ="Format error!! Ex:12:00 or 05:05")]
        //public String Time { get; set; }

        [Required]
     
        public int StartingPointDestinationId { get; set; }

        
        public int EndPointDestinationId { get; set; }

        
        public int RouteAndFareId { get; set; }

        [Required]
        public string DeparturePlace { get; set; }

        public virtual Vehicle Vehicle { get; set; }
        public virtual Committee Committee { get; set; }
        

        [ForeignKey("StartingPointDestinationId")]
        public virtual Destination StartingPointDestination { get; set; }

        [ForeignKey("EndPointDestinationId")]
        public virtual Destination EndPointDestination { get; set; }

        public virtual RouteAndFare RouteAndFare { get; set; }
    }
}
