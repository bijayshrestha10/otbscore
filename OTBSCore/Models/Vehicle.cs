﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.Models
{
    public enum VehicleStatus
    {
        unsuspended,suspended
    }
    public class Vehicle
    {
        public enum VehicleType
        {
            bus,hiace,winger
        }
        //public enum BusSeatPlan
        //{
        //    plan1,plan2,plan3
        //}

        [Key]
        public int VehicleId { get; set; }

        public int CommitteeId { get; set; }

        [RegularExpression(@"^[a-z]{1,3}[0-9]{1,2}[a-z]{1,3}[1-9][0-9]{0,3}$",ErrorMessage ="Error in format! Ex: ba1pa9999")]
        [Required]
        [StringLength(12)]
        [Display(Name ="Vehicle Number")]
        public String VehicleNumber { get; set; }

        [Required]
        [Display(Name = "Type")]
        public VehicleType vehicleType { get; set; }

        //[Display(Name = "Seat Plan")]
        //public BusSeatPlan busSeatplan { get; set; }

        [Required]
        [Display(Name = "vehicle owner")]
        [StringLength(30)]
        public string OwnerName { get; set; }

        [Required]
        [StringLength(30)]
        public string Address { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [StringLength(10)]        
        public string Contact { get; set; }

        [Required]
        [Display(Name ="Extra Charge(%)")]
        public int ExtraCharge { get; set; }


        public VehicleStatus Status { get; set; }

        public virtual Committee Committee { get; set; }
    }
}
