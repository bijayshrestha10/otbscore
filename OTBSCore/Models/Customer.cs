﻿

using System;
using System.ComponentModel.DataAnnotations;

namespace OTBSCore.Models {
    public class Customer {
        [Key]
        [Display(Name="Id")]
        public int CustomerId { get; set; }

        //public int CommitteeId { get; set; }
        //public int VehicleId { get; set; }
        [Display(Name = "Schedule")]
        public int VehicleScheduleId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(10)]
        [Display(Name="Phone Number")]
        public string PhNumber { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Seat Number")]
        public string seatNumber { get; set; }

        [Required]
        [Display(Name = "Ticket Code")]
        public string TicketCode { get; set; }

        [Required]
        [Display(Name = "Cancelled")]
        public Boolean CancelStatus { get; set; }

        [Display(Name = "Booked By")]
        public string BookedBy { get; set; }

        [Display(Name ="Amount Paid")]
        public string AmountPaid { get; set; }

        //public virtual Committee Committee { get; set; }
        //public virtual Vehicle Vehicle { get; set; }
        public virtual VehicleSchedule VehicleSchedule { get; set; }
    }
}