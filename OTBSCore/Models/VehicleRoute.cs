﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.Models
{
    public class VehicleRoute
    {
        public int VehicleRouteId { get; set; }
        public int VehicleId { get; set; }
        public int RoutesAndFaresId { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

    }
}
