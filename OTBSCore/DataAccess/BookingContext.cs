﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OTBSCore.DataAccess;
using OTBSCore.Models;
using Microsoft.EntityFrameworkCore;

using System.Data.Entity.ModelConfiguration;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace OTBSCore.DataAccess
{
    public class BookingContext : IdentityDbContext<ApplicationUser,IdentityRole<int>,int>
    {
        public BookingContext(DbContextOptions<BookingContext> options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Committee> Committees { get; set; }
        public DbSet<Counter> Counters { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Destination> Destinations { get; set; }
        public DbSet<RouteAndFare> RoutesAndFares { get; set; }
        public DbSet<VehicleSchedule> VehicleSchedules { get; set; }
        public DbSet<Seat> Seats { get; set; }
        public DbSet<CommitteeDue> CommitteeDues { get; set; }
        public DbSet<CustomerDue> CustomerDues { get; set; }
        //public DbSet<AdminLogin> AdminLogins { get; set; }
        //public DbSet<AdminRegister> AdminRegistrations { get; set; }


        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    base.OnConfiguring(optionsBuilder);
        //    optionsBuilder.UseMySql("Server=localhost;database=otbs_database;uid=root;password=bikashbijay"); 
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().ToTable("Customer");
            modelBuilder.Entity<Committee>().ToTable("Committee");
            modelBuilder.Entity<Counter>().ToTable("Counter");
            modelBuilder.Entity<Vehicle>().ToTable("Vehicle");
            modelBuilder.Entity<RouteAndFare>().ToTable("RouteAndFare");
            modelBuilder.Entity<VehicleSchedule>().ToTable("VehicleSchedule");
            modelBuilder.Entity<Seat>().ToTable("Seat");
            modelBuilder.Entity<CommitteeDue>().ToTable("CommitteeDue");
            modelBuilder.Entity<CustomerDue>().ToTable("CustomerDue");
            //modelBuilder.Entity<AdminLogin>().ToTable("AdminLogin");
            //modelBuilder.Entity<AdminRegister>().ToTable("AdminRegistration");
            foreach(var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e=>e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            

            base.OnModelCreating(modelBuilder);
            
        }
    }
}
