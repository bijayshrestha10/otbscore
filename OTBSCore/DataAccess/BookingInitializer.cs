﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using OTBSCore.Models;

namespace OTBSCore.DataAccess {
    public class BookingInitializer {
        public static void Initialize (BookingContext context) {
            context.Database.EnsureCreated ();
            var customers = new Customer[] {
                new Customer { Name = "random", Email = "abc@xyz.com", PhNumber = "1234" },
                new Customer { Name = "random2", Email = "abcd@xyz.com", PhNumber = "12345" }
            };
            Console.WriteLine (customers);
            foreach (Customer c in customers) {
                context.Customers.Add (c);
            }
            context.SaveChanges ();

        }
    }
}