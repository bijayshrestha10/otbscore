(function() {
  $(document).on('click', '#addDestinationBtn', function() {
    $('#destinationAddModal').modal();
  });

  var destinationTable = $('#destinationTable').DataTable();
  var allDestinations = [];

  fetchAndPopulateDestinations();

  function fetchAndPopulateDestinations() {
    $.ajax({
      method: 'GET',
      url: '/api/GetDestinations',
      contentType: 'application/json',
      beforeSend: function() {
        $('#destinationTableBody').html(
          '<tr class="odd"><td colspan="6" class="dataTables_empty" valign="top"><i class="fa fa-spinner fa-spin"></i> Loading...</td></tr>'
        );
      },
      success: function(data) {
        if (data && data.length > 0) {
          allDestinations = data;
          data.forEach(function(destination) {
            var actionsHtml =
              '<button class="btn btn-info btnEdit" type="button" data-id="' +
              destination.destinationId +
              '"><i class="fa fa-edit"></i></button>';
            actionsHtml +=
              '&nbsp;&nbsp;<button class="btn btn-danger btnRemove" type="button" data-id="' +
              destination.destinationId +
              '"><i class="fa fa-trash"></i></button>';

            destinationTable.row
              .add([
                destination.destinationId,
                destination.destinationName,
                destination.district,
                actionsHtml
              ])
              .draw(false);
          });
        } else {
          $('#destinationTableBody').html(
            '<tr class="odd"><td colspan="6" class="dataTables_empty" valign="top">No data available in table</td></tr>'
          );
        }
      },
      error: function() {
        $('#destinationTableBody').html(
          '<tr class="odd"><td colspan="6" class="dataTables_empty" valign="top">No data available in table</td></tr>'
        );
      }
    });
  }

  $(document).on('click', '.btnRemove', function() {
    var $this = $(this);
    var id = $this.data('id');
    var removeConfirm = confirm(
      'Are you sure want to delete this destination?'
    );
    if (removeConfirm) {
      $.ajax({
        method: 'DELETE',
        url: '/api/DeleteDestination/' + id,
        contentType: 'application/json',
        beforeSend: function() {
          $this.html('<i class="fa fa-spinner fa-spin"></i>');
          $('.btnRemove').prop('disabled', true);
        },
        success: function() {
          window.location.reload();
        },
        error: function() {
          $this.html('<i class="fa fa-trash"></i>');
          $('.btnRemove').prop('disabled', false);
        }
      });
    }
  });

  $(document).on('click', '.btnEdit', function() {
    var $this = $(this);
    var id = $this.data('id');
    var destination = allDestinations.find(function(d) {
      return d.destinationId === id;
    });
    $('#edit-destinationName').val(destination.destinationName);
    $('#edit-districtName').val(destination.district);
    $('#updateBtn').data('id', destination.destinationId);

    $('#destinationEditModal').modal();
  });

  $('#destinationAddForm').on('submit', function(e) {
    e.preventDefault();

    var destinationName = $('#add-destinationName').val();
    var districtName = $('#add-districtName').val();

    var finalObj = {
      DestinationName: destinationName,
      District: districtName
    };

    $.ajax({
      method: 'POST',
      url: '/api/AddDestination',
      contentType: 'application/json',
      data: JSON.stringify(finalObj),
      beforeSend: function() {
        $('#saveBtn')
          .html('<i class="fa fa-spin fa-spinner"></i> Saving')
          .prop('disabled', true);
      },
      success: function() {
        window.location.reload();
      },
      error: function() {
        $('#saveBtn')
          .html('Save Changes')
          .prop('disabled', false);
      }
    });
  });

  $('#destinationEditForm').on('submit', function(e) {
    e.preventDefault();

    var destinationName = $('#edit-destinationName').val();
    var districtName = $('#edit-districtName').val();
    var destinationId = $('#updateBtn').data('id');

    var finalObj = {
      DestinationId: destinationId,
      DestinationName: destinationName,
      District: districtName
    };

    $.ajax({
      method: 'PUT',
      url: '/api/UpdateDestination',
      contentType: 'application/json',
      data: JSON.stringify(finalObj),
      beforeSend: function() {
        $('#updateBtn')
          .html('<i class="fa fa-spin fa-spinner"></i> Updating')
          .prop('disabled', true);
      },
      success: function() {
        window.location.reload();
      },
      error: function() {
        $('#updateBtn')
          .html('Update Changes')
          .prop('disabled', false);
      }
    });
  });
})();
