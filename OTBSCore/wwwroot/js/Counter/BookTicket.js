﻿function viewSchedule(committeeId) {
  var selectDestination1 = document.getElementById('destination1');
  var selectDestination2 = document.getElementById('destination2');

  var destination1 =
    selectDestination1.options[selectDestination1.selectedIndex].text;
  var destination2 =
    selectDestination2.options[selectDestination2.selectedIndex].text;
  $.ajax({
    url: '/api/getDates',
    type: 'GET',
    data: {
      location1: destination1,
      location2: destination2,
      committeeId: committeeId
    },
    success: function(dates) {
      populateDateTime(dates, committeeId);
    },
    error: function(request, message, error) {
      handleError(request, message, error);
    }
  });
}

var seatsData = [];

function populateDateTime(dates, committeeId) {
  if (dates.length == 0) {
    document.getElementById('dateTime').disabled = true;
    clearVehicleList();
    document.getElementById('selectVehicle').disabled = true;
    clearSeats();
    clearFare();
  } else {
    document.getElementById('dateTime').disabled = false;
    document.getElementById('dateTime').innerHTML = '';
  }
  var i, j;
  var checkSame = 0;
  for (i = 0; i <= dates.length - 2; i++) {
    for (j = i + 1; j <= dates.length - 1; j++) {
      if (
        dates[i].date == dates[j].date &&
        dates[i].time.substr(dates[i].time.length - 8, 8) ==
          dates[j].time.substr(dates[i].time.length - 8, 8)
      ) {
        checkSame = 1;
        console.log('same');
      } else {
      }
    }
    if (checkSame == 0) {
      var a = document.createElement('option');
      a.setAttribute('value', dates[i].date + '#' + dates[i].time);
      a.setAttribute('class', 'dateSelect');
      a.innerHTML = dates[i].date.substr(0, 10);
      a.innerHTML += '&nbsp;&nbsp;&nbsp;&nbsp;';
      a.innerHTML += dates[i].time.substr(11, 5);
      document.getElementById('dateTime').appendChild(a);
    }
    checkSame = 0;
  }
  var a = document.createElement('option');
  a.setAttribute('value', dates[i].date + '#' + dates[i].time);
  a.setAttribute('class', 'dateSelect');
  a.innerHTML = dates[i].date.substr(0, 10);
  a.innerHTML += '&nbsp;&nbsp;&nbsp;&nbsp;';
  a.innerHTML += dates[i].time.substr(11, 5);
  document.getElementById('dateTime').appendChild(a);
  getVehicles(committeeId);
}

function getVehicles(committeeId) {
  var selectedDate = document.getElementById('dateTime');
  clearVehicleList();
  var selectDestination1 = document.getElementById('destination1');
  var selectDestination2 = document.getElementById('destination2');

  var Location1 =
    selectDestination1.options[selectDestination1.selectedIndex].text;
  var Location2 =
    selectDestination2.options[selectDestination2.selectedIndex].text;

  var DateTime = selectedDate.options[selectedDate.selectedIndex].value;
  var Date = DateTime.split('#')[0];
  var Time = DateTime.split('#')[1];

  $.ajax({
    url: '/api/getVehicles',
    type: 'GET',
    data: {
      location1: Location1,
      location2: Location2,
      date: Date,
      time: Time,
      committeeId: committeeId
    },
    dataType: 'JSON',
    success: function(vehicles) {
      populateVehicles(vehicles, committeeId);
    },
    error: function(request, message, error) {
      handleError(request, message, error);
    }
  });
}

function populateVehicles(vehicles, committeeId) {
  if (vehicles.length == 0) {
    document.getElementById('selectVehicle').disabled = true;
  } else {
    document.getElementById('selectVehicle').disabled = false;
    document.getElementById('selectVehicle').innerHTML == '';
  }

  var i, j;
  var checkSame = 0;
  for (i = 0; i <= vehicles.length - 2; i++) {
    for (j = i + 1; j <= vehicles.length - 1; j++) {
      if (vehicles[i].vehicleId == vehicles[j].vehicleId) {
        checkSame = 1;
      } else {
      }
    }
    if (checkSame == 0) {
      var a = document.createElement('option');
      a.setAttribute('value', vehicles[i].vehicleId);
      a.setAttribute('class', 'vehicleSelect');

      a.innerHTML = vehicles[i].vehicleNumber;

      document.getElementById('selectVehicle').appendChild(a);
    }
    checkSame = 0;
  }
  var a = document.createElement('option');
  a.setAttribute('value', vehicles[i].vehicleId);
  a.setAttribute('class', 'vehicleSelect');
  a.innerHTML = vehicles[i].vehicleNumber;

  document.getElementById('selectVehicle').appendChild(a);
  viewSeatInfo(committeeId);
}

function clearVehicleList() {
  var x = document.getElementById('selectVehicle');

  var i;
  for (i = 0; i < x.childNodes.length; i++) {
    x.removeChild(x.childNodes[i]);
  }
  //var b = document.createElement("option");
  //b.innerText = "Choose date and time..";
  //x.appendChild(b);
}

function viewSeatInfo(committeeId) {
  clearSeats();
  var selectDestination1 = document.getElementById('destination1');
  var selectDestination2 = document.getElementById('destination2');

  var Location1 =
    selectDestination1.options[selectDestination1.selectedIndex].text;
  var Location2 =
    selectDestination2.options[selectDestination2.selectedIndex].text;
  var selectedDate = document.getElementById('dateTime');
  var selectedVehicle = document.getElementById('selectVehicle');
  var DateTime = selectedDate.options[selectedDate.selectedIndex].value;
  var Date = DateTime.split('#')[0];
  var Time = DateTime.split('#')[1];
  var VehicleId = Number(
    selectedVehicle.options[selectedVehicle.selectedIndex].value
  );

  $.ajax({
    url: '/api/getSeats',
    type: 'GET',
    dataType: 'JSON',
    data: {
      location1: Location1,
      location2: Location2,
      date: Date,
      time: Time,
      committeeId: committeeId,
      vehicleId: VehicleId
    },
    success: function(seats) {
      if (seats && seats.length > 0) {
        seatsData = seats;
      }
      showSeats(seats);
      getTotalFare();
    },
    error: function(request, message, error) {
      handleError(request, message, error);
    }
  });
}

function showSeats(seats) {
  var bookedNode = document.getElementById('bookedSeatInfo');
  var unbookedNode = document.getElementById('unbookedSeatInfo');
  var labelBooked = document.createElement('label');
  var labelUnbooked = document.createElement('label');
  var b = document.createElement('label');
  var a = document.createElement('label');

  labelBooked.innerHTML = 'Booked Seats:&nbsp;&nbsp;';
  labelUnbooked.innerHTML = 'Unbooked Seats:&nbsp;&nbsp;';
  if (seats.length != 0) {
    var i;
    for (i = 0; i < seats.length; i++) {
      console.log('inside');
      if (seats[i].booked == true) {
        a.innerHTML += seats[i].seatNumber;
        a.innerHTML += '&nbsp;&nbsp;';
        a.setAttribute('value', seats[i].id);
      } else if (seats[i].booked == false) {
        b.innerHTML += seats[i].seatNumber;
        b.innerHTML += '&nbsp;&nbsp;';
        b.setAttribute('value', seats[i].id);
      }
      bookedNode.appendChild(labelBooked);
      bookedNode.appendChild(a);
      unbookedNode.appendChild(labelUnbooked);
      unbookedNode.appendChild(b);
    }
  }
}
function clearSeats() {
  var x = document.getElementById('bookedSeatInfo');
  var y = document.getElementById('unbookedSeatInfo');
  x.innerHTML = '';
  y.innerHTML = '';
}

function Book() {
  clearSeats();
  var selectDestination1 = document.getElementById('destination1');
  var selectDestination2 = document.getElementById('destination2');

  var Location1 =
    selectDestination1.options[selectDestination1.selectedIndex].text;
  var Location2 =
    selectDestination2.options[selectDestination2.selectedIndex].text;
  var selectedDate = document.getElementById('dateTime');
  var selectedVehicle = document.getElementById('selectVehicle');
  var DateTime = selectedDate.options[selectedDate.selectedIndex].value;
  var Date = DateTime.split('#')[0];
  var Time = DateTime.split('#')[1];
  var VehicleId = Number(
    selectedVehicle.options[selectedVehicle.selectedIndex].value
  );
  var SeatNumber = document.getElementById('userSeat').value;
  var Name = document.getElementById('userName').value;
  var phoneNumber = document.getElementById('phone').value;

  var committeeId = document.getElementById('committeeId').value;
  var counterEmail = document.getElementById('counterEmail').value;
  var counterId = document.getElementById('counterId').value;

  var fareLabel = document.getElementById('fare').innerText;
  var fare = fareLabel.substr(12, fareLabel.length - 12);

  var finalObj = {
    location1: Location1,
    location2: Location2,
    date: Date,
    time: Time,
    committeeId: committeeId,
    vehicleId: VehicleId,
    seatNumber: SeatNumber,
    name: Name,
    email: counterEmail,
    PhNumber: phoneNumber,
    BookedBy: counterId,
    AmountPaid: fare
  };

  $.ajax({
    url: '/api/book',
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(finalObj),
    success: function(msg) {
      document.getElementById('msg').innerHTML = msg;
      alert('Your ticket is booked successfully !!');
      window.location.reload();
    },
    error: function(request, message, error) {
      document.getElementById('msg').innerHTML =
        'Error!!Booking not successful! Try Again!!!';
      alert('Error!!Booking not successful! Try Again!!!');
      $('#payment-button').prop('disabled', false);
    }
  });
}

function getTotalFare() {
  var selectDestination1 = document.getElementById('destination1');
  var selectDestination2 = document.getElementById('destination2');

  var Location1 =
    selectDestination1.options[selectDestination1.selectedIndex].text;
  var Location2 =
    selectDestination2.options[selectDestination2.selectedIndex].text;
  var selectedDate = document.getElementById('dateTime');
  var selectedVehicle = document.getElementById('selectVehicle');
  var DateTime = selectedDate.options[selectedDate.selectedIndex].value;
  var Date = DateTime.split('#')[0];
  var Time = DateTime.split('#')[1];
  var VehicleId = Number(
    selectedVehicle.options[selectedVehicle.selectedIndex].value
  );

  var CommitteeId = Number(document.getElementById('committeeId').value);
  var CounterId = Number(document.getElementById('counterId').value);

  $.ajax({
    url: '/api/getTotalFare',
    type: 'GET',
    dataType: 'JSON',
    data: {
      location1: Location1,
      location2: Location2,
      date: Date,
      time: Time,
      committeeId: CommitteeId,
      vehicleId: VehicleId,
      counterId: CounterId
    },
    success: function(fare) {
      showTotalFare(fare);
      $('#userInfo').prop('hidden', false);
    },
    error: function(request, message, error) {
      handleError(request, message, error);
    }
  });
}
function showTotalFare(fare) {
  clearFare();
  var x = document.getElementById('fare');
  x.innerText = 'Total fare= ' + fare;
}
function clearFare() {
  var x = document.getElementById('fare');
  x.innerText = '';
}

$('#ticketForm').on('submit', function(e) {
  e.preventDefault();
  $('#payment-button').prop('disabled', true);
  pay();
});

$(document).on('keyup', '#userSeat', function() {
  var userSeat = $('#userSeat')
    .val()
    .trim()
    .toLowerCase();
  var seatInfo = seatsData.filter(function(seat) {
    return seat.seatNumber === userSeat;
  });
  const isBooked = seatInfo.length === 0 ? false : seatInfo[0].booked;
  if (seatInfo.length === 0) {
    $('#userSeatError')
      .prop('hidden', false)
      .html('This seat does not exist.');
    $('#userSeat').addClass('is-invalid');
    $('#payment-button').prop('disabled', true);
  } else {
    if (isBooked) {
      $('#userSeatError')
        .prop('hidden', false)
        .html('This seat is already booked.');
      $('#userSeat').addClass('is-invalid');
      $('#payment-button').prop('disabled', true);
    } else {
      $('#userSeatError')
        .prop('hidden', true)
        .html('');
      $('#userSeat').removeClass('is-invalid');
      $('#payment-button').prop('disabled', false);
    }
  }
});

//khalti
function pay() {
  var fareLabel = document.getElementById('fare').innerText;

  var fare = Number(fareLabel.substr(12, fareLabel.length - 12));
  var phoneNumber = document.getElementById('phone').value;
  var config = {
    // replace the publicKey with yours
    publicKey: 'test_public_key_906a6888eda74a0cb850b6ed63aaf3bf',
    productIdentity: '1234567890',
    productName: 'Ticket',
    productUrl: 'https://localhost:44351/api/book',
    eventHandler: {
      onSuccess(payload) {
        // hit merchant api for initiating verfication
        console.log(payload);
        var finalObj = { token: payload.token, amount: payload.amount };
        $.ajax({
          url: '/api/verifyPayment',
          method: 'POST',
          data: JSON.stringify(finalObj),
          contentType: 'application/json',
          success: function(message) {
            Book();
          },
          error: function(request, message, error) {
            document.getElementById('msg').innerHTML =
              'There is a problem calling your payment gateway';
          }
        });
      },
      onError(error) {
        console.log(error);
      },
      onClose() {
        console.log('widget is closing');
      }
    }
  };

  var checkout = new KhaltiCheckout(config);

  checkout.show({ amount: fare * 100 });

  //Paste this code anywhere in you body tag
}

function handleError(request, message, error) {
  console.log('Error message ', error);
}
