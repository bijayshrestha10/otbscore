$(document).ready(function() {
  fetchAllDesitnations();
});

var RouteAndFare = {
  RouteAndFareId: 0,
  Destination1: "",
  Destination2: "",
  Distance: 0,
  Amount: 0
};
var destinationOptionHtml =
  '<option value="" selected disabled>Select destination</option>';

// fetch all destination for drop down list
function fetchAllDesitnations() {
  $.ajax({
    url: "/api/GetDestinations",
    type: "GET",
    contentType: "application/json",
    success: function(destinations) {
      destinations.forEach(function(destination) {
        destinationOptionHtml +=
          '<option value="' +
          destination.destinationId +
          '">' +
          destination.destinationName +
          "</option>";
      });
      $("#Destination1").html(destinationOptionHtml);
      $("#Destination2").html(destinationOptionHtml);

      getRouteAndFareList();
    }
  });
}

//get all routes and fares to display
function getRouteAndFareList() {
  //clear the input fields
  $("#Destination1").val("");
  $("#Destination2").val("");
  $("#Amount").val("");
  $("#Distance").val("");
  //call the web api to get the route and fare list
  $.ajax({
    url: "/api/getRoutesAndFares",
    type: "GET",
    datatype: "json",
    success: function(routesAndFares) {
      routeAndFareListSuccess(routesAndFares);
    },
    error: function(request, message, error) {
      handleException(request, message, error);
    }
  });
}

//display all routes and fares returned from api call
function routeAndFareListSuccess(routesAndFares) {
  //iterate over the collection of data
  $("#RouteAndFareTable tbody").remove();
  $.each(routesAndFares, function(index, routeAndFare) {
    //add a row for each route and fare
    routeAndFareAddRow(routeAndFare);
  });
}

//add a row for each row
function routeAndFareAddRow(routeAndFare) {
  //check if table body exists or not
  if ($("#RouteAndFareTable tbody").length == 0) {
    $("#RouteAndFareTable").append("<tbody></tbody>");
  }
  //append row to a table
  $("#RouteAndFareTable tbody").append(buildTableRow(routeAndFare));

  // now select dropdown value
  $("#destination1-" + routeAndFare.routeAndFareId).val(
    routeAndFare.destination1
  );
  $("#destination2-" + routeAndFare.routeAndFareId).val(
    routeAndFare.destination2
  );
}

function buildTableRow(routeAndFare) {
  var newRow =
    "<tr>" +
    "<td>" +
    routeAndFare.routeAndFareId +
    "</td>" +
    "<td><select type='text' id='destination1-" +
    routeAndFare.routeAndFareId +
    "' class='destination1 form-control'  required>" +
    destinationOptionHtml +
    "</select>" +
    "</td>" +
    "<td><select type='text' id='destination2-" +
    routeAndFare.routeAndFareId +
    "' class='destination2 form-control' required>" +
    destinationOptionHtml +
    "</select>" +
    "</td>" +
    "<td><input type='number' id='distance-" +
    routeAndFare.routeAndFareId +
    "' class='distance form-control'  value='" +
    routeAndFare.distance +
    "' required />" +
    "</td>" +
    "<td><input type='number' id='amount-" +
    routeAndFare.routeAndFareId +
    "' class='amount form-control'  value='" +
    routeAndFare.amount +
    "' required />" +
    "</td>" +
    "<td><input type='button' value='Update' class='btn btn-primary' onclick='routeAndFareUpdate(this)'" +
    "data-routeAndFareId='" +
    routeAndFare.routeAndFareId +
    "'" +
    "data-destination1='" +
    routeAndFare.destination1 +
    "'" +
    "data-destination2='" +
    routeAndFare.destination2 +
    "'" +
    "data-distance='" +
    routeAndFare.distance +
    "'" +
    "data-amount='" +
    routeAndFare.amount +
    "'" +
    "/></td>" +
    "<td><input type='button' class='btn btn-danger' value='Delete' onclick='routeAndFareDelete(this)'" +
    "data-routeAndFareId='" +
    routeAndFare.routeAndFareId +
    "'" +
    "/></td>";

  return newRow;
}

function onAddRouteAndFare() {
  var options = {};
  options.url = "/api/addRouteAndFare";
  options.type = "POST";
  var Obj = RouteAndFare;
  Obj.Destination1 = $("#Destination1").val();
  Obj.Destination2 = $("#Destination2").val();
  Obj.Amount = $("#Amount").val();
  Obj.Distance = $("#Distance").val();
  options.data = JSON.stringify(Obj);
  options.datatype = "html";
  options.contentType = "application/json";
  options.success = function(msg) {
    getRouteAndFareList();
    $("#msg").html(msg);
  };
  options.error = function() {
    $("#msg").html("Error while calling the web api");
  };
  $.ajax(options);
}

function routeAndFareUpdate(item) {
  var options = {};
  options.url = "/api/updateRouteAndFare";
  options.type = "PUT";
  var Obj = RouteAndFare;
  Obj.RouteAndFareId = $(item).attr("data-routeAndFareId");
  Obj.Destination1 = $(
    ".destination1",
    $(item)
      .parent()
      .parent()
  ).val();
  Obj.Destination2 = $(
    ".destination2",
    $(item)
      .parent()
      .parent()
  ).val();
  Obj.Amount = $(
    ".amount",
    $(item)
      .parent()
      .parent()
  ).val();
  Obj.Distance = $(
    ".distance",
    $(item)
      .parent()
      .parent()
  ).val();

  options.data = JSON.stringify(Obj);
  options.datatype = "html";
  options.contentType = "application/json";
  options.success = function(msg) {
    getRouteAndFareList();
    $("#msg").html(msg);
  };
  options.error = function() {
    $("#msg").html("Error while calling the web api");
  };
  $.ajax(options);
  console.log(options.data);
}

function routeAndFareDelete(item) {
  var id = $(item).attr("data-routeAndFareId");
  var options = {};
  options.url = "/api/deleteRouteAndFare/" + id;
  options.contentType = "application/json";
  options.type = "DELETE";
  options.success = function(msg) {
    $("#msg").html(msg);
    getRouteAndFareList();
  };
  options.error = function() {
    $("#msg").html("Error while connecting the api");
  };
  $.ajax(options);
}
