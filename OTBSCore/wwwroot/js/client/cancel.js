﻿function cancelTicket() {
    var a = document.getElementById("ticketCode");
    var code = a.value;

    var b = document.getElementById("phoneNumber");
    var mobile = b.value;
    

    $.ajax({
        url: '/api/cancelTicket',
        method: 'GET',

        data: { TicketCode: code, KhaltiMobile:mobile },
        contentType: 'application/json',
        success: function (message) {
            document.getElementById("cancelMessage").innerHTML = message;
        },
        error: function (request, message, error) {
            
                document.getElementById("cancelMessage").innerHTML = "Your information is not correct.";
            
        }
    });
}