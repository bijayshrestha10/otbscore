﻿function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener('input', function(e) {
    var a,
      b,
      i,
      val = this.value;
    /*close any already open lists of autocompleted values*/
    closeAllLists();
    if (!val) {
      return false;
    }
    currentFocus = -1;
    /*create a DIV element that will contain the items (values):*/
    a = document.createElement('DIV');
    a.setAttribute('id', this.id + 'autocomplete-list');
    a.setAttribute('class', 'autocomplete-items');
    /*append the DIV element as a child of the autocomplete container:*/
    this.parentNode.appendChild(a);
    /*for each item in the array...*/
    for (i = 0; i < arr.length; i++) {
      /*check if the item starts with the same letters as the text field value:*/
      if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
        /*create a DIV element for each matching element:*/
        b = document.createElement('DIV');
        /*make the matching letters bold:*/
        b.innerHTML = '<strong>' + arr[i].substr(0, val.length) + '</strong>';
        b.innerHTML += arr[i].substr(val.length);
        /*insert a input field that will hold the current array item's value:*/
        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
        /*execute a function when someone clicks on the item value (DIV element):*/
        b.addEventListener('click', function(e) {
          /*insert the value for the autocomplete text field:*/
          inp.value = this.getElementsByTagName('input')[0].value;

          getDates(
            document.getElementById('destination1').value,
            document.getElementById('destination2').value
          );

          /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
          closeAllLists();
        });
        a.appendChild(b);
      }
      getDates(
        document.getElementById('destination1').value,
        document.getElementById('destination2').value
      );
    }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener('keydown', function(e) {
    var x = document.getElementById(this.id + 'autocomplete-list');
    if (x) x = x.getElementsByTagName('div');
    if (e.keyCode == 40) {
      /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
      currentFocus++;
      /*and and make the current item more visible:*/
      addActive(x);
    } else if (e.keyCode == 38) {
      //up
      /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
      currentFocus--;
      /*and and make the current item more visible:*/
      addActive(x);
    } else if (e.keyCode == 13) {
      /*If the ENTER key is pressed, prevent the form from being submitted,*/
      e.preventDefault();
      if (currentFocus > -1) {
        /*and simulate a click on the "active" item:*/
        if (x) x[currentFocus].click();
      }
    }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = x.length - 1;
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add('autocomplete-active');
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove('autocomplete-active');
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
    var x = document.getElementsByClassName('autocomplete-items');
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener('click', function(e) {
    closeAllLists(e.target);
  });
}

//get list of all the destinations from api/getdestinations

var destinationNames = new Array();
var seatsData = [];

function getDestinations() {
  $.ajax({
    url: '/api/getDestinations',
    type: 'GET',
    dataType: 'JSON',
    success: function(destinations) {
      //store destinations in an array
      var i;
      for (i = 0; i <= destinations.length - 1; i++) {
        destinationNames[i] = destinations[i].destinationName;
      }
    },
    error: function(request, message, error) {
      handleError(request, message, error);
    }
  });
  return destinationNames;
}

var dates = new Array();
function getDates(destination1, destination2) {
  $.ajax({
    url: '/api/getDates',
    data: { location1: destination1, location2: destination2 },
    type: 'GET',
    dataType: 'JSON',
    success: function(dates) {
      populateDateTime(dates);
    },
    error: function(request, message, error) {
      handleError(request, message, error);
    }
  });
}

function populateDateTime(dates) {
  clearSelectList();
  if (dates.length == 0) {
    document.getElementById('selectDate').disabled = true;
    document.getElementById('selectCommittee').disabled = true;
    document.getElementById('selectVehicle').disabled = true;

    clearSelectList();
    clearCommitteeList();
    clearVehicleList();
  } else {
    document.getElementById('selectDate').disabled = false;
    document.getElementById('selectDate').innerHTML = '';
    document.getElementById('selectCommittee').innerHTML = '';
    document.getElementById('selectVehicle').innerHTML = '';
    clearSelectList();
    clearCommitteeList();
    clearVehicleList();
  }
  var i, j;
  var checkSame = 0;
  for (i = 0; i <= dates.length - 2; i++) {
    for (j = i + 1; j <= dates.length - 1; j++) {
      if (
        dates[i].date == dates[j].date &&
        dates[i].time.substr(dates[i].time.length - 8, 8) ==
          dates[j].time.substr(dates[i].time.length - 8, 8)
      ) {
        checkSame = 1;
      } else {
      }
    }
    if (checkSame == 0) {
      var a = document.createElement('option');
      a.setAttribute('value', dates[i].date + '#' + dates[i].time);
      a.setAttribute('class', 'dateSelect');
      a.innerHTML = dates[i].date.substr(0, 10);
      a.innerHTML += '&nbsp;&nbsp;&nbsp;&nbsp;';
      a.innerHTML += dates[i].time.substr(11, 5);
      document.getElementById('selectDate').appendChild(a);
    }
    checkSame = 0;
  }
  var a = document.createElement('option');
  a.setAttribute('value', dates[i].date + '#' + dates[i].time);
  a.setAttribute('class', 'dateSelect');
  a.innerHTML = dates[i].date.substr(0, 10);
  a.innerHTML += '&nbsp;&nbsp;&nbsp;&nbsp;';
  a.innerHTML += dates[i].time.substr(11, 5);
  document.getElementById('selectDate').appendChild(a);
  onDateChange();
}

function clearSelectList() {
  var x = document.getElementById('selectDate');
  var i;
  for (i = 0; i < x.childNodes.length; i++) {
    x.removeChild(x.childNodes[i]);
  }
  //var b = document.createElement("option");
  //b.innerText = "Choose date and time..";
  //x.appendChild(b);
}

function onDateChange() {
  var selectedDate = document.getElementById('selectDate');
  var DateTime = selectedDate.options[selectedDate.selectedIndex].value;
  var Date = DateTime.split('#')[0];
  var Time = DateTime.split('#')[1];
  var Location1 = document.getElementById('destination1').value;
  var Location2 = document.getElementById('destination2').value;

  $.ajax({
    url: '/api/getCommittees',
    type: 'GET',
    data: {
      location1: Location1,
      location2: Location2,
      date: Date,
      time: Time
    },
    dataType: 'JSON',
    success: function(committees) {
      clearCommitteeList();
      populateCommittees(committees);
    },
    error: function(request, message, error) {
      handleError(request, message, error);
    }
  });
}

function populateCommittees(committees) {
  clearCommitteeList();
  if (committees.length == 0) {
    document.getElementById('selectCommittee').disabled = true;
    clearCommitteeList();
    clearFare();
  } else {
    document.getElementById('selectCommittee').disabled = false;
    document.getElementById('selectCommittee').innerHTML == '';
    clearCommitteeList();
  }

  var i, j;
  var checkSame = 0;
  for (i = 0; i <= committees.length - 2; i++) {
    for (j = i + 1; j <= committees.length - 1; j++) {
      if (committees[i].committeeId == committees[j].committeeId) {
        checkSame = 1;
      } else {
      }
    }
    if (checkSame == 0) {
      var a = document.createElement('option');
      a.setAttribute('value', committees[i].committeeId);
      a.setAttribute('class', 'committeeSelect');
      a.innerHTML = committees[i].name;

      document.getElementById('selectCommittee').appendChild(a);
    }
    checkSame = 0;
  }
  var a = document.createElement('option');
  a.setAttribute('value', committees[i].committeeId);
  a.setAttribute('class', 'committeeSelect');
  a.innerHTML = committees[i].name;

  document.getElementById('selectCommittee').appendChild(a);
  onCommitteeChange();
}

function clearCommitteeList() {
  var x = document.getElementById('selectCommittee');
  var i;
  for (i = 0; i < x.childNodes.length; i++) {
    x.removeChild(x.childNodes[i]);
  }
  //var b = document.createElement("option");
  //b.innerText = "choose committee";
  //x.selectedIndex=0;
  //x.appendChild(b);
}

function onCommitteeChange() {
  var selectedDate = document.getElementById('selectDate');
  var selectedCommittee = document.getElementById('selectCommittee');
  var CommitteeId = Number(
    selectedCommittee.options[selectedCommittee.selectedIndex].value
  );
  var DateTime = selectedDate.options[selectedDate.selectedIndex].value;
  var Date = DateTime.split('#')[0];
  var Time = DateTime.split('#')[1];
  var Location1 = document.getElementById('destination1').value;
  var Location2 = document.getElementById('destination2').value;

  $.ajax({
    url: '/api/getVehicles',
    type: 'GET',
    data: {
      location1: Location1,
      location2: Location2,
      date: Date,
      time: Time,
      committeeId: CommitteeId
    },
    dataType: 'JSON',
    success: function(vehicles) {
      clearVehicleList();
      populateVehicles(vehicles);
      getTotalFare();
    },
    error: function(request, message, error) {
      handleError(request, message, error);
    }
  });
}

function populateVehicles(vehicles) {
  clearVehicleList();
  if (vehicles.length == 0) {
    document.getElementById('selectVehicle').disabled = true;
    clearVehicleList();
    clearFare();
  } else {
    document.getElementById('selectVehicle').disabled = false;
    document.getElementById('selectVehicle').innerHTML == '';
    clearVehicleList();
  }

  var i, j;
  var checkSame = 0;
  for (i = 0; i <= vehicles.length - 2; i++) {
    for (j = i + 1; j <= vehicles.length - 1; j++) {
      if (vehicles[i].vehicleId == vehicles[j].vehicleId) {
        checkSame = 1;
      } else {
      }
    }
    if (checkSame == 0) {
      var a = document.createElement('option');
      a.setAttribute('value', vehicles[i].vehicleId);
      a.setAttribute('class', 'vehicleSelect');

      a.innerHTML = vehicles[i].vehicleNumber;

      document.getElementById('selectVehicle').appendChild(a);
    }
    checkSame = 0;
  }
  var a = document.createElement('option');
  a.setAttribute('value', vehicles[i].vehicleId);
  a.setAttribute('class', 'vehicleSelect');
  a.innerHTML = vehicles[i].vehicleNumber;

  document.getElementById('selectVehicle').appendChild(a);
}

function clearVehicleList() {
  var x = document.getElementById('selectVehicle');
  var i;
  for (i = 0; i < x.childNodes.length; i++) {
    x.removeChild(x.childNodes[i]);
  }
  clearFare();
}

$('#seatInfoForm').on('submit', function(e) {
  e.preventDefault();
  viewSeatInfo();
});

$(document).on('keyup', '#userSeat', function() {
  var userSeat = $('#userSeat')
    .val()
    .trim()
    .toLowerCase();
  var seatInfo = seatsData.filter(function(seat) {
    return seat.seatNumber === userSeat;
  });
  const isBooked = seatInfo.length === 0 ? false : seatInfo[0].booked;
  if (seatInfo.length === 0) {
    $('#userSeatError')
      .prop('hidden', false)
      .html('This seat does not exist.');
    $('#userSeat').addClass('is-invalid');
    $('#payment-button').prop('disabled', true);
  } else {
    if (isBooked) {
      $('#userSeatError')
        .prop('hidden', false)
        .html('This seat is already booked.');
      $('#userSeat').addClass('is-invalid');
      $('#payment-button').prop('disabled', true);
    } else {
      $('#userSeatError')
        .prop('hidden', true)
        .html('');
      $('#userSeat').removeClass('is-invalid');
      $('#payment-button').prop('disabled', false);
    }
  }
});

function viewSeatInfo() {
  clearSeats();
  var Location1 = document.getElementById('destination1').value;
  var Location2 = document.getElementById('destination2').value;
  var selectedCommittee = document.getElementById('selectCommittee');
  var selectedDate = document.getElementById('selectDate');
  var selectedVehicle = document.getElementById('selectVehicle');
  var DateTime = selectedDate.options[selectedDate.selectedIndex].value;
  var Date = DateTime.split('#')[0];
  var Time = DateTime.split('#')[1];
  var CommitteeId = Number(
    selectedCommittee.options[selectedCommittee.selectedIndex].value
  );
  var VehicleId = Number(
    selectedVehicle.options[selectedVehicle.selectedIndex].value
  );

  $.ajax({
    url: '/api/getSeats',
    type: 'GET',
    dataType: 'JSON',
    data: {
      location1: Location1,
      location2: Location2,
      date: Date,
      time: Time,
      committeeId: CommitteeId,
      vehicleId: VehicleId
    },
    success: function(seats) {
      if (seats && seats.length > 0) {
        seatsData = seats;
      }
      showSeats(seats);
      $('#userInfo').prop('hidden', false);
    },
    error: function(request, message, error) {
      handleError(request, message, error);
    }
  });
}

function showSeats(seats) {
  var bookedNode = document.getElementById('bookedSeatInfo');
  var unbookedNode = document.getElementById('unbookedSeatInfo');
  var labelBooked = document.createElement('label');
  var labelUnbooked = document.createElement('label');
  var b = document.createElement('label');
  var a = document.createElement('label');

  labelBooked.innerHTML = 'Booked Seats:&nbsp;&nbsp;';
  labelUnbooked.innerHTML = 'Unbooked Seats:&nbsp;&nbsp;';
  if (seats.length != 0) {
    var i;
    for (i = 0; i < seats.length; i++) {
      if (seats[i].booked == true) {
        a.innerHTML += seats[i].seatNumber;
        a.innerHTML += '&nbsp;&nbsp;';
        a.setAttribute('value', seats[i].id);
      } else if (seats[i].booked == false) {
        b.innerHTML += seats[i].seatNumber;
        b.innerHTML += '&nbsp;&nbsp;';
        b.setAttribute('value', seats[i].id);
      }
      bookedNode.appendChild(labelBooked);
      bookedNode.appendChild(a);
      unbookedNode.appendChild(labelUnbooked);
      unbookedNode.appendChild(b);
    }
  }
}
function clearSeats() {
  var x = document.getElementById('bookedSeatInfo');
  var y = document.getElementById('unbookedSeatInfo');
  x.innerHTML = '';
  y.innerHTML = '';
}

function Book() {
  //clearSeats();
  var Location1 = document.getElementById('destination1').value;
  var Location2 = document.getElementById('destination2').value;
  var selectedCommittee = document.getElementById('selectCommittee');
  var selectedDate = document.getElementById('selectDate');
  var selectedVehicle = document.getElementById('selectVehicle');
  var DateTime = selectedDate.options[selectedDate.selectedIndex].value;
  var Date = DateTime.split('#')[0];
  var Time = DateTime.split('#')[1];
  var CommitteeId = Number(
    selectedCommittee.options[selectedCommittee.selectedIndex].value
  );
  var VehicleId = Number(
    selectedVehicle.options[selectedVehicle.selectedIndex].value
  );
  var SeatNumber = document.getElementById('userSeat').value;
  var Name = document.getElementById('userName').value;
  var Email = document.getElementById('email').value;
  var phoneNumber = document.getElementById('phone').value;
  var fareLabel = document.getElementById('fare').innerText;
  var fare = fareLabel.substr(12, fareLabel.length - 12);

  var finalObj = {
    location1: Location1,
    location2: Location2,
    date: Date,
    time: Time,
    committeeId: CommitteeId,
    vehicleId: VehicleId,
    seatNumber: SeatNumber,
    name: Name,
    email: Email,
    PhNumber: phoneNumber,
    BookedBy: 'self',
    AmountPaid: fare
  };

  $.ajax({
    url: '/api/book',
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(finalObj),
    success: function(msg) {
      document.getElementById('msg').innerHTML = msg;
      alert(msg);
      window.location.reload();
    },
    error: function(request, message, error) {
      document.getElementById('msg').innerHTML =
        msg;
      alert(msg);
      $('#payment-button').prop('disabled', false);
    }
  });
}

function getTotalFare() {
  var Location1 = document.getElementById('destination1').value;
  var Location2 = document.getElementById('destination2').value;
  var selectedCommittee = document.getElementById('selectCommittee');
  var selectedDate = document.getElementById('selectDate');
  var selectedVehicle = document.getElementById('selectVehicle');
  var DateTime = selectedDate.options[selectedDate.selectedIndex].value;
  var Date = DateTime.split('#')[0];
  var Time = DateTime.split('#')[1];
  var CommitteeId = Number(
    selectedCommittee.options[selectedCommittee.selectedIndex].value
  );
  var VehicleId = Number(
    selectedVehicle.options[selectedVehicle.selectedIndex].value
  );

  $.ajax({
    url: '/api/getTotalFare',
    type: 'GET',
    dataType: 'JSON',
    data: {
      location1: Location1,
      location2: Location2,
      date: Date,
      time: Time,
      committeeId: CommitteeId,
      vehicleId: VehicleId
    },
    success: function(fare) {
      showTotalFare(fare);
    },
    error: function(request, message, error) {
      handleError(request, message, error);
    }
  });
}
function showTotalFare(fare) {
  clearFare();
  var x = document.getElementById('fare');
  x.innerText = 'Total fare= ' + fare;
  $('#fare').data('fare', fare);
}
function clearFare() {
  var x = document.getElementById('fare');
  x.innerText = '';
}
/*initiate the autocomplete function on the "myInput" element, and pass along the dest array as possible autocomplete values:*/
autocomplete(document.getElementById('destination1'), getDestinations());
autocomplete(document.getElementById('destination2'), getDestinations());

//khalti client side integration

$('#bookTicketForm').on('submit', function(e) {
  e.preventDefault();
  $('#payment-button').prop('disabled', true);
  pay();
});

//Paste this code anywhere in you body tag
function pay() {
  var fare = Number($('#fare').data('fare'));
  var phoneNumber = document.getElementById('phone').value;
  var config = {
    // replace the publicKey with yours
    publicKey: 'test_public_key_906a6888eda74a0cb850b6ed63aaf3bf',
    productIdentity: '1234567890',
    productName: 'Ticket',
    productUrl: 'https://localhost:44351/api/book',
    eventHandler: {
      onSuccess(payload) {
        // hit merchant api for initiating verfication
          var finalObj = { token: payload.token, amount: payload.amount };
          debugger;
        $.ajax({
          url: '/api/verifyPayment',
          method: 'POST',
          data: JSON.stringify(finalObj),
          contentType: 'application/json',
          success: function(message) {
            Book();
          },
          error: function(request, message, error) {
            alert('There is a problem calling your payment gateway');
          }
        });
      },
      onError(error) {},
      onClose() {}
    }
  };

  var checkout = new KhaltiCheckout(config);
    debugger;
  checkout.show({ amount: fare * 10 });

  //Paste this code anywhere in you body tag
}
