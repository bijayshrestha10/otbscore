﻿function deleteVehicle(vehicleId) {
    if (confirm("Press Ok to delete the specified Vehicle")) {
        window.location.href = '@Url.Action("DeleteVehicle","VehicleData")/' + vehicleId;
    }
}

function unsuspendVehicle(vehicleId:Number) {
    if (confirm("Press ok to unsuspend the vehicle")) {
        window.location.href = '@Url.Action("UnsuspendVehicle","VehicleData")/' + vehicleId;
    }

}

function suspendVehicle(vehicleId) {
    if (confirm("Press ok to suspend the vehicle")) {
        window.location.href = '@Url.Action("SuspendVehicle","VehicleData")/' + vehicleId;
    }
}