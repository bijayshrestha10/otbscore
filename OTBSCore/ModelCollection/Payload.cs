﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.ModelCollection
{
    public class Payload
    {
        public string Token { get; set; }
        public int Amount { get; set; }
    }
}
