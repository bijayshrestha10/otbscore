﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.ModelCollection
{
    public class CustomerViewModel
    {
        public string Location1 { get; set; }

        public string Location2 { get; set; }

        public DateTime Date { get; set; }

        public DateTime Time { get; set; }

        public int CommitteeId { get; set; }

        public int VehicleId { get; set; }

        
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(10)]
        [Display(Name = "Phone Number")]
        public string PhNumber { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string SeatNumber { get; set; }

        public string BookedBy { get; set; }

        public string AmountPaid { get; set; }


    }
}
