﻿using OTBSCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.ModelCollection
{
    public class CombinedModel
    {
        public Customer Customer { get; set; }
        public Committee Committee { get; set; }
        public Counter Counter { get; set; }
        public Vehicle Vehicle { get; set; }
    }
}
