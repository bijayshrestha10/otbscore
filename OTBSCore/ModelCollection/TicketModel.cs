﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.ModelCollection
{
    public class TicketModel
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string CommitteeName { get; set; }
        public string VehicleNumber { get; set; }
        public string SeatNumber { get; set; }
        public string TicketCode { get; set; }
        public string AmountPaid { get; set; }
        public string DeparturePlace { get; set; }
        
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public string Date { get; set; }

        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh:mm tt}")]
        public string Time { get; set; }

    }
}
