﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OTBSCore.AdminAccounts;
using OTBSCore.DataAccess;
using OTBSCore.Models;

namespace OTBSCore {
    public class Startup {
        public Startup (IConfiguration configuration) {
            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder ();
            Configuration = (IConfiguration) configurationBuilder.SetBasePath (Directory.GetCurrentDirectory ())
                .AddJsonFile ("appsettings.json")
                .AddEnvironmentVariables ()
                .Build ();
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            services.Configure<CookiePolicyOptions> (options => {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            //using (var context = new BookingContext())
            //{
            //    context.Database.EnsureCreated();
            //    if(!context.Customers.Any())
            //    {
            //        context.Customers.Add(new Models.Customer
            //        {
            //            Name = "abc",
            //            PhNumber = "1223",
            //            Email = "sjdfk@djf.com"
            //        });
            //    }
            //    context.SaveChanges();
            //}
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDbContext<BookingContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddIdentity<ApplicationUser, IdentityRole<int>>()
                .AddEntityFrameworkStores<BookingContext>()
                .AddDefaultTokenProviders();






            //password strength setting
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 6;

              
            });



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IHostingEnvironment env,IServiceProvider services) {
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            } else {
                app.UseExceptionHandler ("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts ();
            }

            app.UseHttpsRedirection ();
            app.UseStaticFiles ();
            app.UseCookiePolicy ();
            app.UseAuthentication();

            app.UseMvc (routes => {
                routes.MapRoute (
                    name: "default",
                    template: "{controller=Client}/{action=Index}/{id?}");
            });
            CreateUserRoles(services).Wait();
        }

        private async Task CreateUserRoles(IServiceProvider serviceProvider)
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole<int>>>();
            var UserManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();

            var roleCheckAdmin = await RoleManager.RoleExistsAsync("Admin");
            if(!roleCheckAdmin)
            {
               await RoleManager.CreateAsync(new IdentityRole<int>("Admin"));
            }

            var roleCheckCommittee = await RoleManager.RoleExistsAsync("Committee");
            if (!roleCheckCommittee)
            {
                await RoleManager.CreateAsync(new IdentityRole<int>("Committee"));
            }

            var roleCheckCounter = await RoleManager.RoleExistsAsync("Counter");
            if (!roleCheckCounter)
            {
                await RoleManager.CreateAsync(new IdentityRole<int>("Counter"));
            }

            if (UserManager.FindByNameAsync("bijay.shrestha1").Result == null)
            {
                ApplicationUser user = new ApplicationUser
                {
                    UserName = "bijay.shrestha1",
                    Email = "shresthabijay63@gmail.com",
                    EmailConfirmed = true,
                    PhoneNumber = "9847109260"

                };
                var result = UserManager.CreateAsync(user, "Bijay.shrestha1").Result;
                if (result.Succeeded)
                {
                    await UserManager.AddToRoleAsync(user, "Admin");
                }
            }
        }      
    }
}