﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.AdminAccounts
{
    public class ForgotResetPassword
    {
        [Required]
        [StringLength(50)]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(50,ErrorMessage ="should be less than 30 characters"),MinLength(8,ErrorMessage ="should be more than 8 characters")]
        [Display(Name ="New Password")]
        public string Password { get; set; }


        [Required]
        [DataType(DataType.Password)]
        [Compare("Password",ErrorMessage ="Passwords do not match")]
        [Display(Name ="Confirm password")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
}
