﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OTBSCore.DataAccess;

using OTBSCore.Models;

namespace OTBSCore.APIs
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class RouteAndFareController : ControllerBase
    {
        private BookingContext _context;

        public RouteAndFareController(BookingContext context)
        {
            _context = context;
        }

        [Route("~/api/getRoutesAndFares")]
        [HttpGet]
        public async Task<ICollection<RouteAndFare>> getRoutesAndFares()
        {
            return await _context.RoutesAndFares.ToListAsync();
        }

        [Route("~/api/getRouteAndFare/{id}")]
        [HttpGet]
        public async Task<RouteAndFare> getRouteAndFare(int id)
        {
            return await _context.RoutesAndFares.FindAsync(id);
        }


        [Route("~/api/addRouteAndFare")]
        [HttpPost]
        public async Task<IActionResult> addRouteAndFare([FromBody] RouteAndFare routeAndFare)
        {
            if(ModelState.IsValid)
            {
                var check1 = await _context.RoutesAndFares.Where(a => a.Destination1 == routeAndFare.Destination1).Where(b => b.Destination2 == routeAndFare.Destination2).FirstOrDefaultAsync();
                var check2 = await _context.RoutesAndFares.Where(a => a.Destination1 == routeAndFare.Destination2).Where(b => b.Destination2 == routeAndFare.Destination1).FirstOrDefaultAsync();

                if (check1 == null && check2 == null)
                {

                    _context.RoutesAndFares.Add(routeAndFare);
                    _context.SaveChanges();
                    return new ObjectResult("Route and fare added successfully");
                }

                else
                {
                    return new ObjectResult("This route already exists");
                }
            }
            return new ObjectResult("Route and fare not added");
        }

        [Route("~/api/UpdateRouteAndFare")]
        [HttpPut]
        public IActionResult UpdateRouteAndFare([FromBody] RouteAndFare routeAndFare)
        {
            if (ModelState.IsValid)
            {
                _context.RoutesAndFares.Update(routeAndFare);
                _context.SaveChanges();
                return new ObjectResult("Route and fare modified successfully");
            }
            return new ObjectResult("Error!! not modified");
        }

        [Route("~/api/DeleteRouteAndFare/{id}")]
        [HttpDelete]
        public IActionResult DeleteRouteAndFare(int id)
        {
            _context.RoutesAndFares.Remove( _context.RoutesAndFares.Find(id));
            _context.SaveChanges();
            return new ObjectResult("Route and fare Deleted Successfully");

        }
    }
}