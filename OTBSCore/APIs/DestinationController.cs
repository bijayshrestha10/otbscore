﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OTBSCore.DataAccess;
using OTBSCore.Models;

namespace OTBSCore.APIs {
    [Produces ("application/json")]
    [Route ("api/destination")]
    public class DestinationController : ControllerBase {
        private BookingContext _context;
        public DestinationController (BookingContext context) {
            _context = context;
        }

        [Route ("~/api/GetDestinations")]
        [HttpGet]
        public async Task<ICollection<Destination>> GetDestinations () {
            return await _context.Destinations.ToListAsync ();
        }

        [Route ("~/api/GetDestination/{id}")]
        [HttpGet]
        public async Task<Destination> GetDestination (int id) {
            return await _context.Destinations.FindAsync (id);
        }

        [Route ("~/api/AddDestination")]
        [HttpPost]
        public IActionResult AddDestination ([FromBody] Destination destination) {
            if (ModelState.IsValid) {
                _context.Destinations.Add (destination);
                _context.SaveChanges ();
                return new ObjectResult ("Destination added successfully!!");

            }
            return new ObjectResult ("Destination not added");
        }

        [Route ("~/api/UpdateDestination")]
        [HttpPut]
        public IActionResult UpdateDestination ([FromBody] Destination destination) {
            if (ModelState.IsValid) {
                _context.Destinations.Update (destination);
                _context.SaveChanges ();
                return new ObjectResult ("Destination modified successfully");
            }
            return new ObjectResult ("Error!! not modified");
        }

        [Route ("~/api/DeleteDestination/{id}")]
        [HttpDelete]
        public IActionResult DeleteDestination (int id) {
            _context.Destinations.Remove (_context.Destinations.Find (id));
            _context.SaveChanges ();
            return new ObjectResult ("Destination Deleted Successfully");

        }
    }
}