﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using OTBSCore.DataAccess;
using OTBSCore.ModelCollection;
using OTBSCore.Models;



namespace OTBSCore.APIs
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private BookingContext _context;

        public ClientController(BookingContext context)
        {
            _context = context;
        }

        [Route("~/api/getDates")]
        [HttpGet]
        public async Task<List<VehicleSchedule>> getDates(String location1, String location2, int? committeeId)
        {
            var currentDate = DateTime.Now.Date;
            var currentTime = DateTime.Now.TimeOfDay;
            if (committeeId == null)
            {
               IEnumerable<VehicleSchedule> vehicleSchedule =await _context.VehicleSchedules.Where(a => a.StartingPointDestination.DestinationName == location1).Where(b => b.EndPointDestination.DestinationName == location2).ToListAsync();
                IEnumerable<VehicleSchedule> newSchedule = new List<VehicleSchedule>();
                foreach (VehicleSchedule schedule in vehicleSchedule)
                {
                    if(currentDate<schedule.Date)
                    {
                        
                        newSchedule= newSchedule.Concat(new[] { schedule });
                    }
                    else if(currentDate==schedule.Date)
                    {
                        if(currentTime<schedule.Time.TimeOfDay)
                        {
                            newSchedule = newSchedule.Concat(new[] { schedule });
                        }
                    }
                }

                return newSchedule.ToList();
            }
            else
            {
                IEnumerable<VehicleSchedule> vehicleSchedule=await _context.VehicleSchedules.Where(a => a.StartingPointDestination.DestinationName == location1).Where(b => b.EndPointDestination.DestinationName == location2).Where(c => c.CommitteeId == committeeId).ToListAsync();
                IEnumerable<VehicleSchedule> newSchedule = new List<VehicleSchedule>();

                foreach (VehicleSchedule schedule in vehicleSchedule)
                {
                    if (currentDate < schedule.Date)
                    {
                        newSchedule = newSchedule.Concat(new[] { schedule });
                    }
                    else if (currentDate == schedule.Date)
                    {
                        if (currentTime < schedule.Time.TimeOfDay)
                        {
                            newSchedule = newSchedule.Concat(new[] { schedule });
                        }
                    }
                }
                return newSchedule.ToList();
            }
                 
        }

        [Route("~/api/getCommittees")]
        [HttpGet]
        public async Task<List<Committee>> getCommittees(String location1, String location2, DateTime date, DateTime time)
        {
            List<int> committeeIds = await _context.VehicleSchedules.Where(a => a.StartingPointDestination.DestinationName == location1).Where(b => b.EndPointDestination.DestinationName == location2).Where(c => c.Date == date).Where(d => d.Time.TimeOfDay == time.TimeOfDay).Select(e => e.CommitteeId).ToListAsync();
            List<Committee> committees = new List<Committee>();
            foreach (int id in committeeIds)
            {
                committees.Add(_context.Committees.Where(s => s.CommitteeId == id).FirstOrDefault());
            }
            return committees;
        }
        [Route("~/api/getVehicles")]
        [HttpGet]
        public async Task<List<Vehicle>> getVehicles(String location1, String location2, DateTime date, DateTime time, int committeeId)
        {
            List<int> vehicleIds = await _context.VehicleSchedules.Where(a => a.StartingPointDestination.DestinationName == location1).Where(b => b.EndPointDestination.DestinationName == location2).Where(c => c.Date == date).Where(d => d.Time.TimeOfDay == time.TimeOfDay).Where(e => e.CommitteeId == committeeId).Select(f => f.VehicleId).ToListAsync();
            List<Vehicle> vehicles = new List<Vehicle>();
            foreach (int id in vehicleIds)
            {
                vehicles.Add(_context.Vehicles.Where(s => s.VehicleId == id).FirstOrDefault());
            }
            return vehicles;
        }

        [Route("~/api/getSeats")]
        [HttpGet]
        public async Task<List<Seat>> getSeats(String location1, String location2, DateTime date, DateTime time, int committeeId, int vehicleId)
        {
            int vehicleScheduleId = await _context.VehicleSchedules.Where(a => a.StartingPointDestination.DestinationName == location1).Where(b => b.EndPointDestination.DestinationName == location2).Where(c => c.Date == date).Where(d => d.Time.TimeOfDay == time.TimeOfDay).Where(e => e.CommitteeId == committeeId).Where(f => f.VehicleId == vehicleId).Select(g => g.VehicleScheduleId).FirstOrDefaultAsync();
            return await _context.Seats.Where(h => h.VehicleScheduleId == vehicleScheduleId).ToListAsync();
        }

        [Route("~/api/getTotalFare")]
        [HttpGet]
        public async Task<String> getTotalFare(String location1, String location2, DateTime date, DateTime time, int committeeId, int vehicleId, int? counterId)
        {
            int vehicleScheduleId = await _context.VehicleSchedules.Where(a => a.StartingPointDestination.DestinationName == location1).Where(b => b.EndPointDestination.DestinationName == location2).Where(c => c.Date == date).Where(d => d.Time.TimeOfDay == time.TimeOfDay).Where(e => e.CommitteeId == committeeId).Where(f => f.VehicleId == vehicleId).Select(g => g.VehicleScheduleId).FirstOrDefaultAsync();
            int routeAndFareId = await _context.VehicleSchedules.Where(a => a.VehicleScheduleId == vehicleScheduleId).Select(b => b.RouteAndFareId).FirstOrDefaultAsync();
            int totalFare = await _context.RoutesAndFares.Where(a => a.RouteAndFareId == routeAndFareId).Select(b => b.Amount).FirstOrDefaultAsync();
            int extraCharge = await _context.Vehicles.Where(a => a.VehicleId == vehicleId).Select(b => b.ExtraCharge).FirstOrDefaultAsync();

            if (counterId == null)
            {
                float totalFareAmount = (float)totalFare + ((float)extraCharge / 100) * (float)totalFare;
                return totalFareAmount.ToString();
            }
            else if (counterId != null)
            {
                int counterCommission = await _context.Counters.Where(a => a.CounterId == counterId).Select(b => b.Commission).FirstOrDefaultAsync();
                float totalFareAmount = (float)totalFare + ((float)extraCharge / 100) * (float)totalFare + ((float)counterCommission / 100) * (float)totalFare;
                return totalFareAmount.ToString();
            }
            return 0.ToString();
        }

        [Route("~/api/Book")]
        [HttpPost]
        public async Task<IActionResult> Book([FromBody] CustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                int thisVehicleScheduleId = _context.VehicleSchedules.Where(a => a.StartingPointDestination.DestinationName == model.Location1)
                .Where(b => b.EndPointDestination.DestinationName == model.Location2).Where(c => c.Date == model.Date)
                .Where(d => d.Time.TimeOfDay == model.Time.TimeOfDay).Where(e => e.VehicleId == model.VehicleId).Where(f => f.CommitteeId == model.CommitteeId).Select(g => g.VehicleScheduleId).FirstOrDefault();

                int thisSeatId = _context.Seats.Where(i => i.SeatNumber.ToUpper() == model.SeatNumber.ToUpper()).Where(j => j.VehicleScheduleId == thisVehicleScheduleId).Select(k => k.Id).FirstOrDefault();

                String thisSeatNumber = _context.Seats.Where(a => a.Id == thisSeatId).Select(b => b.SeatNumber).FirstOrDefault();

                String code = DateTime.Now.Ticks.ToString();

                var customer = new Customer()
                {
                    VehicleScheduleId = thisVehicleScheduleId,
                    Name = model.Name,
                    Email = model.Email,
                    PhNumber = model.PhNumber,
                    seatNumber = thisSeatNumber,
                    TicketCode = code,
                    CancelStatus = false,
                    BookedBy = model.BookedBy,
                    AmountPaid = model.AmountPaid
                   


                };
                Seat seat = _context.Seats.Where(a => a.Id == thisSeatId).FirstOrDefault();
                if (seat.booked == true)
                {
                    return new ObjectResult("This seat is already taken..Select another seat");
                }

                seat.booked = true;


                _context.Customers.Add(customer);
                _context.Seats.Update(seat);



                int destination1Id = await _context.VehicleSchedules.Where(a => a.VehicleScheduleId == thisVehicleScheduleId).Select(b => b.StartingPointDestinationId).FirstOrDefaultAsync();
                int destination2Id = await _context.VehicleSchedules.Where(a => a.VehicleScheduleId == thisVehicleScheduleId).Select(b => b.EndPointDestinationId).FirstOrDefaultAsync();
                var from = await _context.Destinations.Where(a => a.DestinationId == destination1Id).Select(b => b.DestinationName).FirstOrDefaultAsync();
                var to = await _context.Destinations.Where(a => a.DestinationId == destination2Id).Select(b => b.DestinationName).FirstOrDefaultAsync();
                DateTime date = await _context.VehicleSchedules.Where(a => a.VehicleScheduleId == thisVehicleScheduleId).Select(b => b.Date).FirstOrDefaultAsync();
                DateTime time = await _context.VehicleSchedules.Where(a => a.VehicleScheduleId == thisVehicleScheduleId).Select(b => b.Time).FirstOrDefaultAsync();
                var committeeName = await _context.Committees.Where(a => a.CommitteeId == model.CommitteeId).Select(b => b.Name).FirstOrDefaultAsync();
                var ticket = new TicketModel()
                {
                    Name = model.Name,
                    PhoneNumber = model.PhNumber,
                    Email = model.Email,
                    From = from,
                    To = to,
                    VehicleNumber = await _context.Vehicles.Where(a => a.VehicleId == model.VehicleId).Select(b => b.VehicleNumber).FirstOrDefaultAsync(),
                    SeatNumber = model.SeatNumber,
                    Date = date.ToString("yyyy/MM/dd"),
                    Time = time.ToString("hh:mm tt"),
                    TicketCode = code,
                    AmountPaid = model.AmountPaid,
                    CommitteeName = committeeName,
                    DeparturePlace = await _context.VehicleSchedules.Where(a => a.VehicleScheduleId == thisVehicleScheduleId).Select(b => b.DeparturePlace).FirstOrDefaultAsync()
                };

                CommitteeDue committeeDue =await  _context.CommitteeDues.Where(a => a.CommitteeId == model.CommitteeId).Where(b => b.VehicleScheduleId == thisVehicleScheduleId).FirstOrDefaultAsync();
                var dueSum = committeeDue.DueAmount;
                var amtPaid = int.Parse(model.AmountPaid);
                committeeDue.DueAmount = dueSum + amtPaid;
                _context.CommitteeDues.Update(committeeDue);

                //creating pdf and sending to customer


                try
                {
                    SendTicketController.sendPdfMail(ticket);
                }catch(Exception e)
                {
                    return new ObjectResult("There is a problem sending ticket to your mail. Please try again!");
                }
                var resultCustomer = _context.SaveChanges();
                var resultSeat = _context.SaveChanges();
                return new ObjectResult("Successfully booked!! check your mail for ticket");
            }

            return new ObjectResult("Booking Unsuccessful!!");

        }




        [Route("~/api/cancelTicket")]
        [HttpGet]
        public IActionResult cancelTicket(String TicketCode,String KhaltiMobile)
        {
            int scheduleId = _context.Customers.Where(a => a.TicketCode == TicketCode).Select(b => b.VehicleScheduleId).FirstOrDefault();
            String seatNumber = _context.Customers.Where(a => a.TicketCode == TicketCode).Select(b => b.seatNumber).FirstOrDefault();

            Customer customer = _context.Customers.Where(a => a.TicketCode == TicketCode).FirstOrDefault();
            customer.CancelStatus = true;

            Seat seat = _context.Seats.Where(a => a.VehicleScheduleId == scheduleId).Where(b => b.SeatNumber == seatNumber).FirstOrDefault();
            seat.booked = false;

            DateTime date = _context.VehicleSchedules.Where(a => a.VehicleScheduleId == scheduleId).Select(b => b.Date).FirstOrDefault();
            DateTime currentDate = DateTime.Now.Date;

            DateTime time = _context.VehicleSchedules.Where(a => a.VehicleScheduleId == scheduleId).Select(b => b.Time).FirstOrDefault();
            DateTime currentTime = DateTime.Now;

            if (currentDate < date)
            {
                //we can cancel ticket if current date is less than scheduled date
                _context.Customers.Update(customer);
                _context.Seats.Update(seat);

                var customerDue = new CustomerDue()
                {
                    CustomerId = customer.CustomerId,
                    Date = date,
                    Cleared = false,
                    PhNumber=KhaltiMobile,
                    DueAmount= int.Parse(customer.AmountPaid)
                };
                _context.CustomerDues.Add(customerDue);
                CommitteeDue committeeDue = _context.CommitteeDues.Where(a => a.VehicleScheduleId == scheduleId).FirstOrDefault();
                committeeDue.DueAmount = committeeDue.DueAmount - int.Parse(customer.AmountPaid);
                _context.CommitteeDues.Update(committeeDue);
                _context.SaveChanges();
                return new ObjectResult("Ticket cancelled successfully");

            }
            else if (currentDate == date)
            {
                if (currentTime.AddHours(2) < time)
                {
                    //if date is same, time should be evaluated
                    _context.Customers.Update(customer);
                    _context.Seats.Update(seat);

                    var customerDue = new CustomerDue()
                    {
                        CustomerId = customer.CustomerId,
                        Date = date,
                        Cleared = false,
                        PhNumber = KhaltiMobile,
                        DueAmount = int.Parse(customer.AmountPaid)
                    };
                    _context.CustomerDues.Add(customerDue);

                    CommitteeDue committeeDue = _context.CommitteeDues.Where(a => a.VehicleScheduleId == scheduleId).FirstOrDefault();
                    committeeDue.DueAmount = committeeDue.DueAmount - int.Parse(customer.AmountPaid);
                    _context.CommitteeDues.Update(committeeDue);
                    _context.SaveChanges();
                    return new ObjectResult("Ticket cancelled successfully");
                }
            }

            else
            {
                return new ObjectResult("Ticket cancel is unsuccessful!!Time limit already reached");
            }

            return new ObjectResult("Internal Error occurred while cancelling the ticket");

        }



        [Route("~/api/verifyPayment")]
        [HttpPost]
        public async Task<String> verifyPayment([FromBody] Payload payload)
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Key", "test_secret_key_1889a56af0fa4c54a20dfb10e5cda207");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri("https://khalti.com/");
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "api/v2/payment/verify/");
                request.Content = new StringContent("{\"token\":\""+ payload.Token +"\",\"amount\":"+ payload.Amount +"}",Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.SendAsync(request);
                var responseString = await response.Content.ReadAsStringAsync();
                return responseString;
            }
        }
    }
}