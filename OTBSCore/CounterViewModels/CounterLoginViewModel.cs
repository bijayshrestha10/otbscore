﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OTBSCore.CounterViewModels
{
    public class CounterLoginViewModel
    {
        [Required]
        [StringLength(50, ErrorMessage = "set maximum length to 50"), MinLength(8, ErrorMessage = "set minimum length to 8")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(50, ErrorMessage = "maximum length of password is 50"), MinLength(8, ErrorMessage = "password should be at least 8 characters long")]
        public string Password { get; set; }
    }
}
