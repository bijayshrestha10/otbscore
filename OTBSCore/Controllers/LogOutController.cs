﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OTBSCore.Models;

namespace OTBSCore.Controllers
{
    public class LogOutController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        public LogOutController(SignInManager<ApplicationUser> signInManager,UserManager<ApplicationUser> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }
        public async Task<IActionResult> Index()
        {
            
            await _signInManager.SignOutAsync();
            ICollection<string> roleNames =await  _userManager.GetRolesAsync(await _userManager.FindByIdAsync( _userManager.GetUserId(HttpContext.User)));
            var roleName = roleNames.FirstOrDefault();


            if (roleName == "Admin")
                return RedirectToAction("Index", "Admin");

            if (roleName == "Committee")
                return RedirectToAction("Index", "Committee");

            if (roleName == "Counter")
                return RedirectToAction("Index", "Counter");

            return View("Error");
        }
    }
}