﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
//using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using OTBSCore.DataAccess;
using OTBSCore.ModelCollection;
//using MailKit.Net.Smtp;
using MailKit.Security;
using System.Net.Mail;

namespace OTBSCore
{
    public class SendTicketController
    {
        public static void sendPdfMail(TicketModel ticket)
        {
           
            StringBuilder sb = new StringBuilder();
            sb.Append("<html><head></head><body><h1 align='center'>");
            sb.Append(ticket.CommitteeName);
            sb.Append("</h1><br /><br />");
            sb.Append("<h3 align='center'>Ticket Invoice</h3><br />");
            sb.Append("<table>");
            sb.AppendFormat(@"<tr><td>Name</td><td>{0}</td></tr>", ticket.Name);
            sb.AppendFormat(@"<tr><td>Phone Number</td><td>{0}</td></tr>", ticket.PhoneNumber);
            sb.AppendFormat(@"<tr><td>Email</td><td>{0}</td></tr>", ticket.Email);
            sb.AppendFormat(@"<tr><td>From</td><td>{0}</td></tr>", ticket.From);
            sb.AppendFormat(@"<tr><td>To</td><td>{0}</td></tr>", ticket.To);
            sb.AppendFormat(@"<tr><td>Vehicle Number</td><td>{0}</td></tr>", ticket.VehicleNumber);
            sb.AppendFormat(@"<tr><td>Seat Number</td><td>{0}</td></tr>", ticket.SeatNumber);
            sb.AppendFormat(@"<tr><td>Date</td><td>{0}</td></tr>", ticket.Date);
            sb.AppendFormat(@"<tr><td>Time</td><td>{0}</td></tr>", ticket.Time);
            sb.AppendFormat(@"<tr><td>Ticket Code</td><td>{0}</td></tr>", ticket.TicketCode);
            sb.AppendFormat(@"<tr><td>Amount Paid</td><td>{0}</td></tr>", ticket.AmountPaid);
            sb.AppendFormat(@"<tr><td>DeparturePlace</td><td>{0}</td></tr>", ticket.DeparturePlace);
            sb.Append("</table></body></html>");

            StringReader sr = new StringReader(sb.ToString());

            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();

                MailMessage mm = new MailMessage("otbsmessenger@gmail.com", ticket.Email);
                mm.Subject = "Ticket";
                mm.Body = "Print this ticket invoice";
                mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "TicketInvoice.pdf"));
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential();
                NetworkCred.UserName = "otbsmessenger@gmail.com";
                NetworkCred.Password = "Otbscore1234";
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);


                //var message = new MimeMessage();
                //message.To.Add(new MailboxAddress(ticket.Email));
                //message.From.Add(new MailboxAddress("otbsmessenger@gmail.com"));
                //message.Subject = "Ticket";
                
                
                

                //using (var client = new SmtpClient())
                //{
                //    client.Connect("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
                //    client.AuthenticationMechanisms.Remove("XOAUTH2");
                //    client.Authenticate("otbsmessenger@gmail.com", "Otbscore1234");
                //    client.Send(message);
                //    client.Disconnect(true);
                //}

            }
        }
    }

}
