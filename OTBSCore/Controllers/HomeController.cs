﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OTBSCore.Models;
using OTBSCore.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using OTBSCore.ModelCollection;

namespace OTBSCore.Controllers
{
    public class HomeController : Controller
    {
        private readonly BookingContext _context;
        public HomeController(BookingContext context)
        {
            _context = context;
        }
        [HttpPost]
        public IActionResult Register([Bind("CustomerId,CommitteeId,VehicleId,Name,PhNumber,Email")] Customer customer)
        {
            
            _context.Add(customer);
            _context.SaveChanges();
            

            return Content("successfully added to the database");



        }




        public IActionResult Index()
        {
            ViewData["CommitteeId"] = new SelectList(_context.Set<Committee>(), "CommitteeId", "Name");
            ViewData["VehicleId"] = new SelectList(_context.Set<Vehicle>(), "VehicleId", "VehicleNumber");

            return View();


        }

        [ResponseCache (Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error () {
            return View (new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}