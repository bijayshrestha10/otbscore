﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OTBSCore.DataAccess;

namespace OTBSCore.Controllers
{
    public class TestController : Controller
    {
        private BookingContext _context;
        public TestController(BookingContext context)
        {
            _context=context;
        }
        public IActionResult Index()
        {
            DateTime date = _context.VehicleSchedules.Where(a=>a.VehicleScheduleId==3).Select(b=>b.Date).FirstOrDefault();
            DateTime currentDate = DateTime.Now.Date;

            DateTime time = _context.VehicleSchedules.Where(a => a.VehicleScheduleId == 1).Select(b => b.Time).FirstOrDefault();
            DateTime currentTime = DateTime.Now;
            currentTime= currentTime.AddMinutes(30);

            return Content((time - currentTime).ToString());
            if (currentTime < time)
            {
                return Content("good");
            }
            return Content("not good");

            return Content(currentTime.TimeOfDay.ToString());
            //return Content((date-currentDate).ToString());
        }
    }
}