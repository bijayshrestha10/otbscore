﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using OTBSCore.AdminAccounts;
using OTBSCore.DataAccess;
using OTBSCore.ModelCollection;
using OTBSCore.Models;

namespace OTBSCore.Controllers
{
    public class AdminController : Controller
    {

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly BookingContext _context;

        public AdminController(UserManager<ApplicationUser> userManager,SignInManager<ApplicationUser> signInManager,BookingContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(AdminLoginModel model)
        {
            if(ModelState.IsValid)
            {
                
                var loginResults = await _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe,lockoutOnFailure:false);
                if (loginResults.Succeeded)
                {
                    return RedirectToAction("Index","LoggedIn");
                }
                else
                {
                    ModelState.AddModelError(String.Empty, "Invalid Login Information");
                    ViewData["IncorrectPassword"] = "Incorrect Password";
                    return View(model);
                }
                
            }
            return View(model);
        }


        [Authorize(Roles = "Admin")]
        public IActionResult Register()
        {
            return View();
        }


        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(AdminRegisterModel model)
        {
            if(ModelState.IsValid)
            {
                var identityUser = new ApplicationUser
                {
                    UserName = model.Username,
                    Email=model.Email
                    
                    
                };
                var checkUser = await _userManager.FindByNameAsync(model.Username);
                if(checkUser!=null)
                {
                    return Content("Username already taken");
                }
                var identityResults = await _userManager.CreateAsync(identityUser, model.Password);
                
                if(identityResults.Succeeded)
                {
                    await _userManager.AddToRoleAsync(await _userManager.FindByNameAsync(model.Username), "Admin");
                    //await _signInManager.SignInAsync(identityUser,isPersistent:false);
                    //return RedirectToAction("Index", "LoggedIn");

                    //send confirmation email to the address to confirm account
                    var user = await _userManager.FindByNameAsync(model.Username);
                    var code =await  _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var codeHtmlVersion = HttpUtility.UrlEncode(code);
                    var callbackUrl = Url.Action("ConfirmAccount", "Admin", new { userId = user.Id, code = codeHtmlVersion }, 
                        protocol: HttpContext.Request.Scheme);

                    var message = new MimeMessage();
                    message.To.Add(new MailboxAddress(model.Email));
                    message.From.Add(new MailboxAddress("Online Ticket Booking System", "otbsmessenger@gmail.com"));
                    message.Subject = "Confirm your account";
                    message.Body = new TextPart("html")
                    {
                        Text = "Please confirm your account by clicking this " + $"<a href='{callbackUrl}'>link</a>"
                    };

                  
                    using (var client = new SmtpClient())
                        {
                            client.Connect("smtp.gmail.com", 587, false);
                            client.Authenticate("otbsmessenger@gmail.com", "Otbscore1234");
                            client.Send(message);
                            client.Disconnect(true);
                        } 

                        return Content("check your email to confirm your account");
                }
                else
                {
                    ModelState.AddModelError(String.Empty, "Error in creating a user");
                    //return View(model);
                    return Content("register unsuccessful");
                }

            }
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmAccount(string userId, string code)
        {
            if(userId==null || code==null)
            {
                return View("Error");
            }
            var user =await _userManager.FindByIdAsync(userId);
            if(user==null)
            {
                return View("Error");
            }
            else
            {
                var codeHtmlDecoded = HttpUtility.UrlDecode(code);
               var result=await _userManager.ConfirmEmailAsync(user,codeHtmlDecoded);
                if(!result.Succeeded)
                {
                    return Content("Email not confirmed");
                }
                return View("ConfirmAccount");
            }
        }

        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPassword model)
        {

            if(ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Username);
                if(user==null)
                {
                    return Content("No user with this username");
                }
                if(user.Email!=model.Email)
                {
                    return Content("Username and email do not match");
                }
                if(user!=null &&(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    var code =await _userManager.GeneratePasswordResetTokenAsync(user);
                    var codeHtmlVersion = HttpUtility.UrlEncode(code);
                    var callbackUrl = Url.Action("ForgotResetPassword", "Admin", new { userId = user.Id, code = codeHtmlVersion },
                        protocol: HttpContext.Request.Scheme);

                    /*
                     * send email to user's email address for redirecting to the reset password page
                     */
                    var message = new MimeMessage();
                    message.To.Add(new MailboxAddress(model.Email));
                    message.From.Add(new MailboxAddress("Online Ticket Booking System", "otbsmessenger@gmail.com"));
                    message.Subject="Forgot password confirmation";
                    message.Body = new TextPart("html")
                    {
                        Text = "Click on this"+ $"<a href='{callbackUrl}'>link</a> to reset password" 
                    };


                //configure and send email
                    using (var client = new SmtpClient())
                    {
                        client.Connect("smtp.gmail.com", 587, false);
                        client.Authenticate("otbsmessenger@gmail.com", "Otbscore1234");
                        client.Send(message);
                        client.Disconnect(true);

                    }

                    return View("ForgotPasswordConfirmation");
                }
            }
            return View(model);
        }

       
        [Authorize(Roles ="Admin")]
        public IActionResult ResetPassword()
        {
            return View();
        }


        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPassword model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Username);
                if(user==null)
                {
                    return Content("No user with this email");
                }

                var verifyPassword=_userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, model.OldPassword);
                if(verifyPassword==PasswordVerificationResult.Failed)
                {
                    return Content("Old password is not valid");
                }
                var code =await _userManager.GeneratePasswordResetTokenAsync(user);
                
                var result = await _userManager.ResetPasswordAsync(user, code, model.Password);
                if(result.Succeeded)
                {
                    return Content("Reset password successful");
                }

            }


            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Admin");
        }

        [HttpGet]
        public async Task<IActionResult> ForgotResetPassword(string userId,string code)
        {
            var user = await _userManager.FindByIdAsync(userId);
            ViewData["Email"] = user.Email;
            ViewData["Username"] = user.UserName;
            ViewData["Code"] = code;
            return View();
        }
    
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotResetPassword(ForgotResetPassword model)
        {
            if(ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Username);
                if(user==null)
                {
                    return Content("No user with this id");
                }
                if(!(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    return Content("Email not confirmed so cannot reset password");
                }

                var codeHtmlDecoded = HttpUtility.UrlDecode(model.Code);
                var resetPasswordResult = await _userManager.ResetPasswordAsync(user, codeHtmlDecoded, model.Password);
                if(resetPasswordResult.Succeeded)
                {
                    return Content("Reset password successful");
                }
                else
                {
                    return Content("Reset password unsuccessful");
                }

            }
            return Content("Model state not valid");          
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ViewCommitteeList()
        {
            return View(await _context.Committees.ToListAsync());
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ViewCommitteeDetail(int id)
        {
            return View(await _context.Committees.Where(a => a.CommitteeId == id).FirstOrDefaultAsync());
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> VerifyCommittee(int id)
        {
            var committee = await _context.Committees.Where(a => a.CommitteeId == id).FirstOrDefaultAsync();
            committee.status = Status.verified;
            _context.Committees.Update(committee);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(ViewCommitteeList));
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> SuspendCommittee(int id)
        {
            var committee = await _context.Committees.Where(a => a.CommitteeId == id).FirstOrDefaultAsync();
            committee.status = Status.suspended;
            _context.Committees.Update(committee);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(ViewCommitteeList));
        }
    }
}