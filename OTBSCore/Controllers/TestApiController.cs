using System.Numerics;
using Microsoft.AspNetCore.Mvc;

namespace OTBSCore.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class TestApiController : ControllerBase {

        [HttpGet ("{id}")]
        public long GetRequestId (long id) {
            return id;
        }

        [HttpPost ("{id}")]
        public long PostRequestId (long id) {
            return id;
        }

    }
}