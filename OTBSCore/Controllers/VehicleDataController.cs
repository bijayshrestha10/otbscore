﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OTBSCore.DataAccess;
using OTBSCore.ModelCollection;
using OTBSCore.Models;

namespace OTBSCore.Controllers
{
    
    public class VehicleDataController : Controller
    {
        private BookingContext _context;
        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;
        static String vehicleStatus="";
        
        public VehicleDataController(BookingContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;

            


        }
        public async Task<IActionResult> Index()
        {
            
            var vehicleContext =  _context.Vehicles.Include(c=>c.Committee);
            var loggedInUserName = _userManager.GetUserName(HttpContext.User);
            var loggedInUserId = _context.Committees.Where(s => s.CommitteeUsername == loggedInUserName).Select(t => t.CommitteeId).FirstOrDefault();
            ViewData["userName"] = loggedInUserName;
            ViewData["userId"] = loggedInUserId;
            ViewData["vehicleStatus"] = vehicleStatus;
            ViewData["role"]="Committee";
            vehicleStatus = "";
            //return View(_context.Vehicles.ToListAsync());
            return View( vehicleContext.ToList());

            //return Content("this is working");
        }

        public IActionResult RegisterVehicle()
        {
            var loggedInUsername = _userManager.GetUserName(HttpContext.User);
            var loggedInCommitteeId = _context.Committees.Where(s => s.CommitteeUsername == loggedInUsername).Select(c => c.CommitteeId).FirstOrDefault();
            ViewData["committeeId"] = loggedInCommitteeId;
            ViewData["role"]="Committee";
            //ViewData["seatPlan"] = Vehicle.BusSeatPlan.plan1;
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles ="Committee")]
        public async Task<IActionResult> RegisterVehicle([Bind("CommitteeId,OwnerName,Address,Contact,Email,VehicleNumber,ExtraCharge,Status,vehicleType,busSeatplan")] Vehicle vehicle)
        {
            //var loggedInUsername = _userManager.GetUserName(HttpContext.User);
            //var loggedInCommitteeId = _context.Committees.Where(s => s.CommitteeUsername == loggedInUsername).Select(c => c.CommitteeId).FirstOrDefault();
            //if (ModelState.IsValid)
            //{
            //    var vehicleData = new Vehicle()
            //    {
            //        CommitteeId = loggedInCommitteeId,
            //        OwnerName = vehicle.OwnerName,
            //        Address = vehicle.Address,
            //        Contact = vehicle.Contact,
            //        Email=vehicle.Email,
            //        VehicleNumber=vehicle.VehicleNumber,
            //        ExtraCharge=vehicle.ExtraCharge,
            //        Status=Status.verified,
            //        vehicleType=vehicle.vehicleType,
            //        busSeatplan=vehicle.busSeatplan
            //    };
            //    _context.Vehicles.Add(vehicleData);
            //    await _context.SaveChangesAsync();

            if(ModelState.IsValid)
            {
                vehicle.Status = VehicleStatus.unsuspended;
                _context.Add(vehicle);
                await _context.SaveChangesAsync();
                
                ViewData["message"] = "Vehicle Registered Successfully";
                ViewData["role"] = "Committee";
                return View(vehicle);
            }
            ViewData["message"] = "Not Successful";
            return View(vehicle);
        }


        public async Task<IActionResult> DeleteVehicle(int id)
        {
            var vehicle = await _context.Vehicles.FindAsync(id);
            _context.Vehicles.Remove(vehicle);
            await _context.SaveChangesAsync();
            vehicleStatus = "Vehicle Deleted from the database!! You cannot undo this!";
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult>UnsuspendVehicle(int id)
        {
            var vehicle = await _context.Vehicles.FindAsync(id);
            vehicle.Status = VehicleStatus.unsuspended;
            _context.Update(vehicle);
            await _context.SaveChangesAsync();
            vehicleStatus = "Vehicle Unsuspended";
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> SuspendVehicle(int id)
        {
            var vehicle = await _context.Vehicles.FindAsync(id);
            vehicle.Status = VehicleStatus.suspended;
            _context.Update(vehicle);
            await _context.SaveChangesAsync();
            
            vehicleStatus = "Vehicle suspended";
            return RedirectToAction(nameof(Index));
        }
    }
}

