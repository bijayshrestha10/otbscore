﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OTBSCore.DataAccess;
using OTBSCore.Models;

namespace OTBSCore.Controllers
{
    public class ScheduleController : Controller
    {
        private BookingContext _context;
        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;
        static String scheduleResult="";
        static String addSuccess = "";
        static String routeAndFareIdError = "";
        static String error = "";

        public ScheduleController(BookingContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;


        }
        public async Task<IActionResult> Index()
        {
            var scheduleData = _context.VehicleSchedules.Include(c => c.Committee).Include(c => c.Vehicle).Include(c => c.StartingPointDestination).Include(c => c.EndPointDestination);
            if (_signInManager.IsSignedIn(User))
            {
                var userName = _userManager.GetUserName(HttpContext.User);
                ViewData["userName"] = userName;
                ViewData["scheduleResult"] = scheduleResult;
                ViewData["role"]="Committee";
                scheduleResult = "";
                var committeeId = _context.Committees.Where(s => s.CommitteeUsername == ViewData["userName"].ToString()).Select(t => t.CommitteeId).FirstOrDefault();
                IEnumerable<Vehicle> thisCommitteeVehicles = _context.Vehicles.Where(s => s.CommitteeId == committeeId);
                ViewData["VehicleId"] = new SelectList(thisCommitteeVehicles, "VehicleId", "VehicleNumber");
                ViewData["committeeId"] = committeeId;
                ViewData["success"] = addSuccess;
                ViewData["routeAndFareIdError"] = routeAndFareIdError;
                ViewData["error"] = error;
                error = "";
                routeAndFareIdError = "";
                addSuccess = "";
                return View(scheduleData.ToList());
            }
            else
                return Content("Not authorized to view this page!!!");
        }
        
        public async Task<IActionResult> RemoveSchedule(int id)
        {
            var schedule=await _context.VehicleSchedules.FindAsync(id);
            _context.VehicleSchedules.Remove(schedule);
            await _context.SaveChangesAsync();
            scheduleResult = "Schedule Removed";
            return RedirectToAction(nameof(Index));
        }

        
        [HttpGet]
        [Authorize(Roles ="Committee")]
        public async Task<IActionResult> AddSchedule()
        {
            var user = _userManager.GetUserName(HttpContext.User);
            
            var committeeId = _context.Committees.Where(s => s.CommitteeUsername == user).Select(t=>t.CommitteeId).FirstOrDefault();

            var checkVerification = await _context.Committees.Where(a => a.CommitteeId == committeeId).Select(b => b.status).FirstOrDefaultAsync();
            if (checkVerification == Status.verified)
            {
                ViewData["userName"] = user;
                ViewData["committeeId"] = committeeId;
                IEnumerable<Vehicle> thisCommitteeVehicles = _context.Vehicles.Where(s => s.CommitteeId == committeeId);
                ViewData["VehicleId"] = new SelectList(thisCommitteeVehicles, "VehicleId", "VehicleNumber");
                IEnumerable<Destination> destinations = _context.Destinations;
                ViewData["DestinationId"] = new SelectList(destinations, "DestinationId", "DestinationName");
                ViewData["verificationError"] = "0";
                ViewData["role"]="Committee";

                return View();
            }
            else
            {
                ViewData["role"]="Committee";
                ViewData["verificationError"] = "1";
                return View();
            }
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddSchedule([Bind("CommitteeId,VehicleId,Date,Time,StartingPointDestinationId,EndPointDestinationId,DeparturePlace")]VehicleSchedule schedule)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<Vehicle> thisCommitteeVehicles = _context.Vehicles.Where(s => s.CommitteeId == schedule.CommitteeId);
                ViewData["VehicleId"] = new SelectList(thisCommitteeVehicles, "VehicleId", "VehicleNumber");
                IEnumerable<Destination> destinations = _context.Destinations;
                ViewData["DestinationId"] = new SelectList(destinations, "DestinationId", "DestinationName");
                if (schedule.StartingPointDestinationId == schedule.EndPointDestinationId)
                {
                    error = "Start and End destinations should not be same!!Add again";
                    return RedirectToAction(nameof(Index));
                }

                //var routeAndFareId1 = _context.RoutesAndFares.Where(s => s.Destination1 == schedule.StartingPointDestination.DestinationName).Where(t => t.Destination2 == schedule.EndPointDestination.DestinationName).Select(u => u.RouteAndFareId).FirstOrDefault();
                var routeAndFareId1 = _context.RoutesAndFares.Where(s => s.Destination1 == schedule.StartingPointDestinationId.ToString())
                    .Where(v => v.Destination2 == schedule.EndPointDestinationId.ToString()).Select(z => z.RouteAndFareId).FirstOrDefault();

                //var routeAndFareId2= _context.RoutesAndFares.Where(s => s.Destination1 == schedule.EndPointDestination.DestinationName).Where(t => t.Destination2 == schedule.StartingPointDestination.DestinationName).Select(u => u.RouteAndFareId).FirstOrDefault();

                var routeAndFareId2 = _context.RoutesAndFares.Where(s => s.Destination1 == schedule.EndPointDestinationId.ToString())
                    .Where(v => v.Destination2 == schedule.StartingPointDestinationId.ToString()).Select(z => z.RouteAndFareId).FirstOrDefault();
                if (routeAndFareId1 == 0 && routeAndFareId2 == 0)
                {
                    routeAndFareIdError = "The provided route is not registered in the system";
                    return RedirectToAction(nameof(Index));
                }
                else if (routeAndFareId1 != 0)
                {
                    schedule.RouteAndFareId = routeAndFareId1;
                }
                else if (routeAndFareId2 != 0)
                {
                    schedule.RouteAndFareId = routeAndFareId2;
                }

                else
                {
                    routeAndFareIdError = "Some error occurred while processing the route and fare";
                    ViewData["verificationError"] = "0";
                    return RedirectToAction(nameof(Index));
                }


                if (ModelState.IsValid)
                {
                    await _context.VehicleSchedules.AddAsync(schedule);
                    await _context.SaveChangesAsync();
                    if (_context.Vehicles.Where(a => a.VehicleId == schedule.VehicleId).Select(b => b.vehicleType).FirstOrDefault().ToString() == "bus")
                    {
                        Seat[] seats = new Seat[12];
                        for (int i = 0; i <= 11; i++)
                        {
                            seats[i] = new Seat();
                            seats[i].SeatNumber = ("a" + (i + 1).ToString());
                            seats[i].booked = false;
                            seats[i].VehicleScheduleId = _context.VehicleSchedules.Where(a => a.VehicleId == schedule.VehicleId).Where(c => c.Date == schedule.Date).Where(s => s.Time.TimeOfDay == schedule.Time.TimeOfDay).Select(b => b.VehicleScheduleId).FirstOrDefault();

                            await _context.Seats.AddAsync(seats[i]);

                        }
                        for (int i = 0; i <= 11; i++)
                        {
                            seats[i] = new Seat();
                            seats[i].SeatNumber = ("b" + (i + 1).ToString());
                            seats[i].booked = false;
                            seats[i].VehicleScheduleId = _context.VehicleSchedules.Where(a => a.VehicleId == schedule.VehicleId).Where(c => c.Date == schedule.Date).Where(s => s.Time.TimeOfDay == schedule.Time.TimeOfDay).Select(b => b.VehicleScheduleId).FirstOrDefault();

                            await _context.Seats.AddAsync(seats[i]);

                        }
                    }

                    if (_context.Vehicles.Where(a => a.VehicleId == schedule.VehicleId).Select(b => b.vehicleType).FirstOrDefault().ToString() == "hiace" ||
                        _context.Vehicles.Where(a => a.VehicleId == schedule.VehicleId).Select(b => b.vehicleType).FirstOrDefault().ToString() == "winger")
                    {
                        Seat[] seats = new Seat[4];
                        for (int i = 0; i <= 1; i++)
                        {
                            seats[i] = new Seat();
                            seats[i].SeatNumber = ("a" + (i + 1).ToString());
                            seats[i].booked = false;
                            seats[i].VehicleScheduleId = _context.VehicleSchedules.Where(a => a.VehicleId == schedule.VehicleId).Where(c => c.Date == schedule.Date).Where(s => s.Time.TimeOfDay == schedule.Time.TimeOfDay).Select(b => b.VehicleScheduleId).FirstOrDefault();

                            await _context.Seats.AddAsync(seats[i]);

                        }
                        for (int i = 0; i <= 2; i++)
                        {
                            seats[i] = new Seat();
                            seats[i].SeatNumber = ("b" + (i + 1).ToString());
                            seats[i].booked = false;
                            seats[i].VehicleScheduleId = _context.VehicleSchedules.Where(a => a.VehicleId == schedule.VehicleId).Where(c => c.Date == schedule.Date).Where(s => s.Time.TimeOfDay == schedule.Time.TimeOfDay).Select(b => b.VehicleScheduleId).FirstOrDefault();

                            await _context.Seats.AddAsync(seats[i]);

                        }
                        for (int i = 0; i <= 1; i++)
                        {
                            seats[i] = new Seat();
                            seats[i].SeatNumber = ("c" + (i + 1).ToString());
                            seats[i].booked = false;
                            seats[i].VehicleScheduleId = _context.VehicleSchedules.Where(a => a.VehicleId == schedule.VehicleId).Where(c => c.Date == schedule.Date).Where(s => s.Time.TimeOfDay == schedule.Time.TimeOfDay).Select(b => b.VehicleScheduleId).FirstOrDefault();

                            await _context.Seats.AddAsync(seats[i]);

                        }
                        for (int i = 0; i <= 2; i++)
                        {
                            seats[i] = new Seat();
                            seats[i].SeatNumber = ("d" + (i + 1).ToString());
                            seats[i].booked = false;
                            seats[i].VehicleScheduleId = _context.VehicleSchedules.Where(a => a.VehicleId == schedule.VehicleId).Where(c => c.Date == schedule.Date).Where(s => s.Time.TimeOfDay == schedule.Time.TimeOfDay).Select(b => b.VehicleScheduleId).FirstOrDefault();

                            await _context.Seats.AddAsync(seats[i]);

                        }
                        for (int i = 0; i <= 3; i++)
                        {
                            seats[i] = new Seat();
                            seats[i].SeatNumber = ("e" + (i + 1).ToString());
                            seats[i].booked = false;
                            seats[i].VehicleScheduleId = _context.VehicleSchedules.Where(a => a.VehicleId == schedule.VehicleId).Where(c => c.Date == schedule.Date).Where(s => s.Time.TimeOfDay == schedule.Time.TimeOfDay).Select(b => b.VehicleScheduleId).FirstOrDefault();

                            await _context.Seats.AddAsync(seats[i]);

                        }

                    }


                    var committeeDue = new CommitteeDue()
                    {
                        CommitteeId = schedule.CommitteeId,
                        PhNumber = await _context.Committees.Where(a => a.CommitteeId == schedule.CommitteeId).Select(b => b.PhNumber).FirstOrDefaultAsync(),
                        Date = schedule.Date,
                        DueAmount = 0,
                        Cleared = false,
                        VehicleScheduleId = await _context.VehicleSchedules.Where(a => a.VehicleId == schedule.VehicleId).Where(c => c.Date == schedule.Date).Where(z => z.Time.TimeOfDay == schedule.Time.TimeOfDay).Select(b => b.VehicleScheduleId).FirstOrDefaultAsync()
                    };
                    await _context.CommitteeDues.AddAsync(committeeDue);
                    await _context.SaveChangesAsync();

                    addSuccess = "Successfully added schedule to database";
                    return RedirectToAction(nameof(Index));
                }
                error = "Some unexpected error occured!!Try again";
                return RedirectToAction(nameof(Index));
            }
            else
            {
                error = "The fields should have valid value";
                ViewData["VerificationError"] = "0";
                return RedirectToAction(nameof(Index));
            }
        }
    }
}