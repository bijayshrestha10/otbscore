﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OTBSCore.Models;

namespace OTBSCore.Controllers
{
    public class LoggedInController : Controller
    {
        [Authorize(Roles ="Admin")]
        public IActionResult Index()
        {
            ViewData["role"]="Admin";
            return View() ;
        }
    }
}