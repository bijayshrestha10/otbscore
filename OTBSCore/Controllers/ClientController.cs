﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OTBSCore.DataAccess;
using OTBSCore.Models;

namespace OTBSCore.Controllers
{
    public class ClientController : Controller
    {
        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;
        private BookingContext _context;

        public ClientController(UserManager<ApplicationUser> userManager,SignInManager<ApplicationUser>signInManager,BookingContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            if(_signInManager.IsSignedIn(User))
            {
                var username = _userManager.GetUserName(HttpContext.User);
                var user = await _userManager.FindByNameAsync(username);
                var roles = await _userManager.GetRolesAsync(user);
                var role = roles.FirstOrDefault();

                if(role=="Admin")
                {
                    return RedirectToAction("Index", "LoggedIn");
                }
                else if(role=="Committee")
                {
                    return RedirectToAction("Index", "CommitteeLoggedIn");
                }
                else if(role=="Counter")
                {
                    return RedirectToAction("Index", "CounterLoggedIn");
                }
            }

            return View();
        }

        public async Task<IActionResult> CancelTicket()
        {
            return View();
        }


        public async Task<IActionResult> ViewClientList()
        {
            if (_signInManager.IsSignedIn(User))
            {
                var loggedInUsername = _userManager.GetUserName(User);
                var loggedInUser =await _userManager.FindByNameAsync(loggedInUsername);
                ICollection<String> roles =await _userManager.GetRolesAsync(loggedInUser);
                var userRole = roles.FirstOrDefault();
                ViewData["role"] = userRole;
                if (userRole == "Admin")
                {

                    return View(await _context.Customers.ToListAsync());
                }
                else if(userRole=="Committee")
                {
                    var committeeId = _context.Committees.Where(a => a.CommitteeUsername == loggedInUsername).Select(b=>b.CommitteeId).FirstOrDefault();
                    ViewData["committeeName"] = _context.Committees.Where(a => a.CommitteeId == committeeId).Select(b=>b.Name).FirstOrDefault();

                    return View(await _context.Customers.Where(a => a.VehicleSchedule.CommitteeId == committeeId).ToListAsync());
                }

                else if(userRole=="Counter")
                {
                    var counterId = _context.Counters.Where(a => a.CounterUsername == loggedInUsername).Select(b => b.CounterId).FirstOrDefault();
                    ViewData["counterName"] = _context.Counters.Where(a => a.CounterId == counterId).Select(b => b.Name).FirstOrDefault();
                    ViewData["counterAddress"] =_context.Counters.Where(a => a.CounterId == counterId).Select(b => b.LocalAddress).FirstOrDefault();
                    return View(await _context.Customers.Where(a => a.BookedBy == counterId.ToString()).ToListAsync());
                }

            }
            return Content("You are not logged in to view this data");
        }


        [HttpGet]
        public async Task<IActionResult> GetCustomerDetail(int id)
        {
            var loggedInUsername = _userManager.GetUserName(User);
            var loggedInUser =await _userManager.FindByNameAsync(loggedInUsername);
            ICollection<String> roles =await _userManager.GetRolesAsync(loggedInUser);
            var userRole = roles.FirstOrDefault();
            ViewData["role"] = userRole;
            return View(await _context.Customers.Where(a => a.CustomerId == id).FirstOrDefaultAsync());
        }

        [HttpGet]
        public async Task<IActionResult> GetScheduleDetail(int id)
        {
            var loggedInUsername = _userManager.GetUserName(User);
            var loggedInUser =await _userManager.FindByNameAsync(loggedInUsername);
            ICollection<String> roles =await _userManager.GetRolesAsync(loggedInUser);
            var userRole = roles.FirstOrDefault();
            ViewData["role"] = userRole;
            return View(await _context.VehicleSchedules.Where(a => a.VehicleScheduleId == id).FirstOrDefaultAsync());
        }

        [HttpGet]
        public async Task<IActionResult> GetCounterDetail(int id)
        {
            var loggedInUsername = _userManager.GetUserName(User);
            var loggedInUser =await _userManager.FindByNameAsync(loggedInUsername);
            ICollection<String> roles =await _userManager.GetRolesAsync(loggedInUser);
            var userRole = roles.FirstOrDefault();
            ViewData["role"] = userRole;
            return View(await _context.Counters.Where(a => a.CounterId == id).FirstOrDefaultAsync());
        }

    }
}