﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OTBSCore.DataAccess;

namespace OTBSCore.Controllers
{
    public class CustomerDueController : Controller
    {
        private BookingContext _context;
        static string clearMsg = "";

        public CustomerDueController(BookingContext context)
        {
            _context = context;
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            ViewData["clearMsg"] = clearMsg;
            clearMsg = "";
            return View(await _context.CustomerDues.ToListAsync());
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ClearDue(int id)
        {
            var customerDue = await _context.CustomerDues.Where(a => a.SNo == id).FirstOrDefaultAsync();
            customerDue.Cleared = true;
            _context.CustomerDues.Update(customerDue);
            await _context.SaveChangesAsync();
            clearMsg = "Due Cleared";
            return RedirectToAction(nameof(Index));
        }
    }
}