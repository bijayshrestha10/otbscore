﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OTBSCore.DataAccess;
using OTBSCore.Models;
using OTBSCore.CounterViewModels;
using System.Web;
using MimeKit;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace OTBSCore.Controllers
{
    public class CounterController : Controller
    {
        private readonly BookingContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public CounterController(BookingContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(CounterLoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Username);
                if (user == null)
                {
                    ViewData["notFound"] = "Not Found";
                    return View();
                }

                if (user.EmailConfirmed == true)
                {
                    var userResult = await _signInManager.PasswordSignInAsync(user, model.Password, isPersistent: false, lockoutOnFailure: false);
                    if (userResult.Succeeded)
                    {
                        return RedirectToAction("Index", "CounterLoggedIn");
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid log in information");
                    ViewData["IncorrectPassword"] = "Incorrect Password or email not confirmed";
                    return View();
                }
            }
            return View("Error");
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmAccount(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return Content("Error in confirming");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return Content("No user found");
            }
            var codeHtmlDecoded = HttpUtility.UrlDecode(code);

            var userResult = await _userManager.ConfirmEmailAsync(user, codeHtmlDecoded);
            if (userResult.Succeeded)
            {
                return View();
            }

            return View("Error");


        }

        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(CounterForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Username);
                if (user == null)
                {
                    ViewData["notFound"] = "Username Not Found";
                    return View();
                }
                if (model.Email != await _userManager.GetEmailAsync(user))
                {
                    ViewData["emailError"] = "Email not linked with this username";
                    return View();
                }
                if (await _userManager.IsEmailConfirmedAsync(user))
                {
                    var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                    var codeHtmlVersion = HttpUtility.UrlEncode(code);
                    var callBackUrl = Url.Action("ForgotResetPassword", "Counter",
                        new { id = user.Id, code = codeHtmlVersion }, protocol: HttpContext.Request.Scheme);

                    var message = new MimeMessage();
                    message.To.Add(new MailboxAddress(model.Email));
                    message.From.Add(new MailboxAddress("Online Ticket Booking System", "otbsmessenger@gmail.com"));
                    message.Subject = "Reset Password";
                    message.Body = new TextPart("html")
                    {
                        Text = "Change your password by clicking this " + $"<a href='{callBackUrl}'>link</a>"
                    };

                    using (var client = new SmtpClient())
                    {
                        client.Connect("smtp.gmail.com", 587, false);
                        client.Authenticate("otbsmessenger@gmail.com", "Otbscore1234");
                        client.Send(message);
                        client.Disconnect(true);
                    }
                    return Content("Check your email to change password");
                }

            }
            ModelState.AddModelError(string.Empty, "Invalid information");
            return View();
        }


        [Authorize(Roles = "Admin,Committee,Counter")]

        public IActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(CounterResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Username);
                if (user == null)
                {
                    ViewData["notFound"] = "User name doesn't exist";
                    return View(model);
                }
                if (user.Email != model.Email || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    ViewData["emailError"] = "Email either doesn't match or not verified";
                    return View(model);
                }

                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var resetResult = await _userManager.ResetPasswordAsync(user, code, model.Password);
                if (resetResult.Succeeded)
                {
                    return Content("Reset password successful");
                }
                else
                {
                    return Content("Reset password unsuccessful");
                }

            }
            ModelState.AddModelError(string.Empty, "Invalid informations");
            return View("Error");
        }



        [HttpGet]
        public async Task<IActionResult> ForgotResetPassword(string id, string code)
        {

            if (id == null && code == null)
            {
                return Content("Error in confirming");
            }
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return Content("Error");
            }

            ViewData["Username"] = user.UserName;
            ViewData["Code"] = code;
            ViewData["Email"] = user.Email;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotResetPassword(CounterForgotResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Username);
                if (user == null)
                {
                    return Content("Error");
                }

                var codeHtmlDecoded = HttpUtility.UrlDecode(model.Code);
                var userResult = await _userManager.ResetPasswordAsync(user, codeHtmlDecoded, model.Password);
                if (userResult.Succeeded)
                {
                    return Content("Reset password successful");
                }
                else
                {
                    return Content("Reset password unsuccessful");
                }
            }
            ModelState.AddModelError(string.Empty, "Invalid information");
            return View("Error");
        }

        [Authorize(Roles = "Counter")]
        [HttpGet]
        public async Task<IActionResult> BookTicket()
        {
            var loggedInUsername =  _userManager.GetUserName(HttpContext.User);
            if(loggedInUsername==null)
            {
                return Content("not logged in");
            }
            var user = await _userManager.FindByNameAsync(loggedInUsername);
            ICollection<String> role = await _userManager.GetRolesAsync(user);
           
            if (role.FirstOrDefault() == "Counter")
            {
                int counterId = _context.Counters.Where(a => a.CounterUsername == loggedInUsername).Select(b => b.CounterId).FirstOrDefault();
                
                int committeeId = _context.Counters.Where(a => a.CounterUsername == loggedInUsername).Select(b => b.CommitteeId).FirstOrDefault();
                var committeeName= _context.Committees.Where(a => a.CommitteeId == committeeId).Select(b => b.Name).FirstOrDefault();
                var counterEmail = _context.Counters.Where(a => a.CounterId == counterId).Select(b => b.Email).FirstOrDefault();

                var checkVerification = await _context.Counters.Where(a => a.CounterId == counterId).Select(b => b.status).FirstOrDefaultAsync();
                ViewData["committeeName"] = committeeName;
                ViewData["committeeId"] = committeeId;
                ViewData["counterId"] = counterId.ToString();
                ViewData["counterLocalAddress"] = _context.Counters.Where(a => a.CounterId == counterId).Select(b => b.LocalAddress).FirstOrDefault();
                ViewData["counterEmail"] = counterEmail;
                if (checkVerification == Status.verified)
                {


                   
                    ViewData["destination1"] = new SelectList(_context.Destinations, "DestinationId", "DestinationName");
                    ViewData["destination2"] = new SelectList(_context.Destinations, "DestinationId", "DestinationName");
                    ViewData["checkVerification"] = "0";
                    return View();
                }
                else
                {
                    ViewData["checkVerification"] = "1";
                    return View();
                }
            }
            
            
                return Content("Not logged in as a counter");
            
            
        }

        [Authorize(Roles = "Admin,Committee")]
        [HttpGet]
        public async Task<IActionResult> ViewCounterList()
        {
            if (_signInManager.IsSignedIn(User))
            {
                var loggedInUser = await _userManager.GetUserAsync(HttpContext.User);
                var roles = await _userManager.GetRolesAsync(loggedInUser);
                var role = roles.FirstOrDefault();
                ViewData["role"] = role;
                var committeeName = await _context.Committees.Where(a => a.CommitteeUsername == loggedInUser.UserName).Select(b => b.Name).FirstOrDefaultAsync();
                
                if (role == "Admin")
                {

                    return View(await _context.Counters.ToListAsync());
                }

                if (role == "Committee")
                {
                    ViewData["committeeName"] = committeeName.ToString();
                    var userName = loggedInUser.UserName;
                    var committeeId = await _context.Committees.Where(a => a.CommitteeUsername == userName).Select(b=>b.CommitteeId).FirstOrDefaultAsync();
                    return View(await _context.Counters.Where(a => a.CommitteeId == committeeId).ToListAsync());
                }
            }
            else
            {
                return Content("You are not logged in to view counter list");
            }
            return Content("some error occurred");
        }

        [Authorize(Roles = "Admin,Committee")]
        public async Task<IActionResult> ViewCounterDetail(int id)
        {
            return View(await _context.Counters.Where(a => a.CounterId == id).FirstOrDefaultAsync());
        }

        [Authorize(Roles = "Admin,Committee")]
        public async Task<IActionResult> VerifyCounter(int id)
        {
            var counter = await _context.Counters.Where(a => a.CounterId == id).FirstOrDefaultAsync();
            counter.status = Status.verified;
            _context.Counters.Update(counter);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(ViewCounterList));
        }

        [Authorize(Roles = "Admin,Committee")]
        public async Task<IActionResult> SuspendCounter(int id)
        {
            var counter = await _context.Counters.Where(a => a.CounterId == id).FirstOrDefaultAsync();
            counter.status = Status.suspended;
            _context.Counters.Update(counter);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(ViewCounterList));
        }
    }
}