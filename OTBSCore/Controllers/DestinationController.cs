﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace OTBSCore.Controllers
{
    public class DestinationController : Controller
    {
        [Authorize(Roles ="Admin")]
        public IActionResult Index()
        {
            ViewData["role"]="Admin";
            return View();
        }
    }
}