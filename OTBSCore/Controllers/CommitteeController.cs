﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;
using System.Web;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using OTBSCore.CommitteeViewModels;
using OTBSCore.DataAccess;
using OTBSCore.ModelCollection;
using OTBSCore.Models;

namespace OTBSCore.Controllers
{
    public class CommitteeController : Controller
    {
        private readonly BookingContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public CommitteeController(BookingContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(CommitteeLoginViewModel model)
        {
            if (ModelState.IsValid)
            {
               
                var user = await _userManager.FindByNameAsync(model.Username);
                if (user == null)
                {
                    ViewData["notFound"] = "Not Found";
                    return View();
                }

                if (user.EmailConfirmed == true)
                {

                    var userResult = await _signInManager.PasswordSignInAsync(user, model.Password, isPersistent: false, lockoutOnFailure: false);
                    if (userResult.Succeeded)
                    {
                        return RedirectToAction("Index", "CommitteeLoggedIn");
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid log in information");
                    ViewData["IncorrectPassword"] = "Incorrect Password or email not confirmed";
                    return View();
                }
            }
            return View("Error");
        }


        
        public IActionResult RegisterCommittee()
        {


            return View(); ;

        }


        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RegisterCommittee(CommitteeRegistrationViewModel model)
        {
            if (ModelState.IsValid)
            {

                var user = new ApplicationUser()
                {
                    UserName = model.CommitteeUsername,
                    Email = model.Email

                };
                var checkUser = await _userManager.FindByNameAsync(user.UserName);

                if (checkUser != null)
                {
                    return Content("Username already exists");
                }

                var result =await  _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    
                    await _userManager.AddToRoleAsync(user, "Committee");
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var codeHtmlVersion = HttpUtility.UrlEncode(code);

                    var callbackUrl = Url.Action("ConfirmAccount", "Committee", new { userId = user.Id, code = codeHtmlVersion },
                    protocol: HttpContext.Request.Scheme);

                    var message = new MimeMessage();
                    message.To.Add(new MailboxAddress(model.Email));
                    message.From.Add(new MailboxAddress("Online Ticket Booking System", "otbsmessenger@gmail.com"));
                    message.Subject = "Confirm Account";
                    message.Body = new TextPart("html")
                    {
                        Text = "Confirm your account by clicking this" + $"<a href='{callbackUrl}'>link</a> "
                    };

                    using (var client = new SmtpClient())
                    {
                        client.Connect("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
                        client.AuthenticationMechanisms.Remove("XOAUTH2");
                        client.Authenticate("otbsmessenger@gmail.com", "Otbscore1234");
                        client.Send(message);
                        client.Disconnect(true);
                    }

                    //add details to the committee table


                    var committee = new Committee()
                    {
                        
                        CommitteeUsername = model.CommitteeUsername,
                        Name = model.Name,
                        Email = model.Email,
                        PhNumber = model.PhNumber,
                        District = model.District,
                        Municipality = model.Municipality,
                        LocalAddress = model.LocalAddress,
                        status = Status.unverified


                    };
                    _context.Committees.Add(committee);
                    _context.SaveChanges();

                    return Content("Check your email to confirm account");
                }

            }
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmAccount(string userId, string code)
        {
            if (userId == null ||code==null)
            {
                return Content("Error in confirming");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if(user==null)
            {
                return Content("No user found");
            }
            var codeHtmlDecoded = HttpUtility.UrlDecode(code);

            var userResult = await _userManager.ConfirmEmailAsync(user, codeHtmlDecoded);
            if(userResult.Succeeded)
            {
                return View();
            }

            return View("Error");
            

        }
       
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(CommitteeForgotPasswordViewModel model)
        {
            if(ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Username);
                if (user == null)
                {
                    ViewData["notFound"] = "Username Not Found";
                    return View();
                }
                if(model.Email!=await _userManager.GetEmailAsync(user))
                {
                    ViewData["emailError"] = "Email not linked with this username";
                    return View();
                }
                if (await _userManager.IsEmailConfirmedAsync(user))
                {
                    var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                    var codeHtmlVersion = HttpUtility.UrlEncode(code);
                    var callBackUrl = Url.Action("ForgotResetPassword", "Committee",
                        new { id = user.Id, code = codeHtmlVersion }, protocol: HttpContext.Request.Scheme);

                    var message = new MimeMessage();
                    message.To.Add(new MailboxAddress(model.Email));
                    message.From.Add(new MailboxAddress("Online Ticket Booking System", "otbsmessenger@gmail.com"));
                    message.Subject = "Reset Password";
                    message.Body = new TextPart("html")
                    {
                        Text = "Change your password by clicking this " + $"<a href='{callBackUrl}'>link</a>"
                    };

                    using (var client = new SmtpClient())
                    {
                        client.Connect("smtp.gmail.com", 587, false);
                        client.Authenticate("otbsmessenger@gmail.com", "Otbscore1234");
                        client.Send(message);
                        client.Disconnect(true);
                    }
                    return Content("Check your email to change password");
                }

            }
            ModelState.AddModelError(string.Empty, "Invalid information");
            return View();
        }

       
        [Authorize(Roles ="Admin,Committee")]
        public IActionResult ResetPassword()
        {
            return View();
        }

        [Authorize(Roles = "Committee")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(CommitteeResetPasswordViewModel model)
        {
            if(ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Username);
                if(user==null)
                {
                    ViewData["notFound"] = "User name doesn't exist";
                    return View(model);
                }
                if(user.Email!=model.Email ||!(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    ViewData["emailError"] = "Email either doesn't match or not verified";
                    return View(model);
                }

                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var resetResult = await _userManager.ResetPasswordAsync(user, code, model.Password);
                if(resetResult.Succeeded)
                {
                    return Content("Reset password successful");
                }
                else
                {
                    return Content("Reset password unsuccessful");
                }

            }
            ModelState.AddModelError(string.Empty, "Invalid informations");
            return View("Error");
        }

      

        [HttpGet]
        public async Task<IActionResult> ForgotResetPassword(string id,string code)
        {

            if (id == null && code == null)
            {
                return Content("Error in confirming");
            }
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return Content("Error");
            }

            ViewData["Username"] = user.UserName;
            ViewData["Code"] = code;
            ViewData["Email"] = user.Email;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotResetPassword(ForgotResetPasswordViewModel model)
        {
            if(ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Username);
                if(user==null)
                {
                    return Content("Error");
                }

                var codeHtmlDecoded = HttpUtility.UrlDecode(model.Code);
                var userResult = await _userManager.ResetPasswordAsync(user, codeHtmlDecoded, model.Password);
                if(userResult.Succeeded)
                {
                    return Content("Reset password successful");
                }
                else
                {
                    return Content("Reset password unsuccessful");
                }
            }
            ModelState.AddModelError(string.Empty, "Invalid information");
            return View("Error");
        }


        [Authorize(Roles ="Admin,Committee")]
        public IActionResult RegisterCounter()
        {
            var loggedInUser = _userManager.GetUserAsync(HttpContext.User);
            if (loggedInUser == null)
            {
                return Content("No committee logged in to register committee");
            }
            ViewData["role"]="Committee";
            return View();
        }

        [Authorize(Roles = "Committee")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RegisterCounter(CounterRegistrationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var loggedInUser = _userManager.GetUserName(HttpContext.User);
                var loggedInUserId = _context.Committees.Where(d => d.CommitteeUsername == loggedInUser).Select(c => c.CommitteeId).FirstOrDefault();
                var user = new ApplicationUser()
                {
                    UserName = model.CounterUsername,
                    Email = model.Email

                };
                var checkUser = await _userManager.FindByNameAsync(user.UserName);

                if (checkUser != null)
                {
                    return Content("Username already exists");
                }

                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "Counter");
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var codeHtmlVersion = HttpUtility.UrlEncode(code);

                    var callbackUrl = Url.Action("ConfirmAccount", "Counter", new { userId = user.Id, code = codeHtmlVersion },
                    protocol: HttpContext.Request.Scheme);

                    var message = new MimeMessage();
                    message.To.Add(new MailboxAddress(model.Email));
                    message.From.Add(new MailboxAddress("Online Ticket Booking System", "otbsmessenger@gmail.com"));
                    message.Subject = "Confirm Account";
                    message.Body = new TextPart("html")
                    {
                        Text = "Confirm your account by clicking this" + $"<a href='{callbackUrl}'>link</a> "
                    };

                    using (var client = new SmtpClient())
                    {
                        client.Connect("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
                        client.AuthenticationMechanisms.Remove("XOAUTH2");
                        client.Authenticate("otbsmessenger@gmail.com", "Otbscore1234");
                        client.Send(message);
                        client.Disconnect(true);
                    }

                    //add details to the committee table
                    //var appuser = (_userManager.FindByNameAsync(model.CounterUsername));
                    var counter = new Counter()
                    {
                        
                        Name = model.Name,
                        Email = model.Email,
                        PhNumber = model.PhNumber,
                        District = model.District,
                        Municipality = model.Municipality,
                        LocalAddress = model.LocalAddress,
                        status = Status.unverified,
                        Commission = model.Commission,
                        CommitteeId = loggedInUserId,
                        CounterUsername=model.CounterUsername


                    };
                    _context.Counters.Add(counter);
                    _context.SaveChanges();

                    return Content("Check your email to confirm account");
                }
                else
                {
                    await _userManager.DeleteAsync(user);
                }
            }
            return View();
        }

    }
}