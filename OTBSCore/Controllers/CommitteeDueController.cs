﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OTBSCore.DataAccess;

namespace OTBSCore.Controllers
{
    public class CommitteeDueController : Controller
    {
        private BookingContext _context;
        static string clearMsg = "";

        public CommitteeDueController(BookingContext context)
        {
            _context = context;
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            ViewData["clearMsg"] = clearMsg;
            clearMsg = "";
            return View(await _context.CommitteeDues.ToListAsync());
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ClearDue(int id)
        {
            var committeeDue =await _context.CommitteeDues.Where(a => a.SNo == id).FirstOrDefaultAsync();
            committeeDue.Cleared = true;
             _context.CommitteeDues.Update(committeeDue);
            await _context.SaveChangesAsync();
            clearMsg = "Due Cleared";
            return RedirectToAction(nameof(Index));
        }
    }
}