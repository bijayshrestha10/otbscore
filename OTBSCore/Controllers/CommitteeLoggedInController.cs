﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OTBSCore.Controllers
{
    public class CommitteeLoggedInController : Controller
    {
        public IActionResult Index()
        {
            ViewData["role"]="Committee";
            return View();
        }
    }
}