﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using OTBSCore.DataAccess;
using OTBSCore.Models;

namespace OTBSCore.Controllers
{
    public class VehicleCustomerController : Controller
    {
        private BookingContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public VehicleCustomerController(BookingContext context, UserManager<ApplicationUser> userManager,SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public IActionResult Index()
        {
            IEnumerable<Destination> destinations = _context.Destinations;
            ViewData["DestinationId"] = new SelectList(destinations, "DestinationId", "DestinationName");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GetCustomerListForSchedule([Bind("From,To,Date,Time")] CustomerForSchedule model)
        {
            if (ModelState.IsValid)
            {
                var loggedInUserName = _userManager.GetUserName(HttpContext.User);
                var user = await _userManager.FindByNameAsync(loggedInUserName);
                var roles = await _userManager.GetRolesAsync(user);
                var role = roles.FirstOrDefault();
                if (role == "Committee")
                {
                    var committeeId = _context.Committees.Where(a => a.CommitteeUsername == loggedInUserName).Select(b => b.CommitteeId).FirstOrDefault();
                    var scheduleId = _context.VehicleSchedules.Where(a => a.CommitteeId == committeeId).Where(b => b.Date == model.Date).Where(c => c.Time.TimeOfDay == model.Time.TimeOfDay).Where(d => d.StartingPointDestinationId == model.From).Where(e => e.EndPointDestinationId == model.To).Select(f => f.VehicleScheduleId).FirstOrDefault();
                    var customers = _context.Customers.Where(a => a.VehicleScheduleId == scheduleId).Where(b => b.CancelStatus == false);
                    var from = _context.Destinations.Where(a => a.DestinationId == model.From).Select(b => b.DestinationName).FirstOrDefault();
                    var to = _context.Destinations.Where(a => a.DestinationId == model.To).Select(b => b.DestinationName).FirstOrDefault();
                    //creating pdf doc
                    iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4, 25, 25, 25, 15);

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                        pdfDoc.Open();

                        //Top Heading
                        String Heading1 = string.Format("{0}-{1}  ", from, to);
                        Chunk chunk1 = new Chunk(Heading1, FontFactory.GetFont("Arial", 10, Font.BOLD, BaseColor.BLACK));
                        pdfDoc.Add(chunk1);

                        String Heading2 = string.Format("  {0}, {1}", model.Date.Date, model.Time.TimeOfDay);
                        Chunk chunk2 = new Chunk(Heading2, FontFactory.GetFont("Arial", 5, Font.BOLD, BaseColor.BLACK));
                        pdfDoc.Add(chunk2);

                        //Horizontal Line
                        Paragraph line = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                        pdfDoc.Add(line);

                        //Table
                        PdfPTable table = new PdfPTable(3);
                        table.WidthPercentage = 100;
                        //0=Left, 1=Centre, 2=Right
                        table.HorizontalAlignment = 0;
                        table.SpacingBefore = 20f;
                        table.SpacingAfter = 30f;

                        table.AddCell("Customer Name");
                        table.AddCell("Seat");
                        table.AddCell("Ticket Code");
                        foreach (Customer customer in customers)
                        {

                            table.AddCell(customer.Name);
                            table.AddCell(customer.seatNumber);
                            table.AddCell(customer.TicketCode);

                        }
                        pdfDoc.Add(table);
                        writer.CloseStream = false;
                        pdfDoc.Close();

                        FileStreamResult fileStreamResult = new FileStreamResult(memoryStream, "application/pdf");
                        string fileDownloadName = string.Format("{0} {1} {2}-{3}.pdf", model.Date.Date, model.Time.TimeOfDay, from, to);
                        return File(memoryStream.ToArray(), "application/pdf", fileDownloadName);
                    }


                }
                else
                {
                    return Content("not logged in");
                }
            }
            return RedirectToAction(nameof(Index));
        }
    }
}